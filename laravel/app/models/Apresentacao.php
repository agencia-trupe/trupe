<?php

class Apresentacao extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'apresentacao';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function scopeOrdenado($query)
    {
        return $query->orderBy('ordem', 'asc');
    }
    
    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeNotThis($query, $id)
    {
        return $query->where('id', '!=', $id);
    }
    
    public function cliente()
    {
        return $this->belongsTo('Clientes', 'clientes_id');
    }

    public function itens()
    {
        return $this->belongsToMany('ItensTrabalho', 'apresentacao_item', 'itens_id', 'apresentacao_id')
                    ->orderBy('titulo', 'asc');
    }

    public function imagens()
    {
        return $this->hasMany('ApresentacaoImagens', 'apresentacao_id')->ordenado();
    }    
}
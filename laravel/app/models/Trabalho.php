<?php

class Trabalho extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'trabalhos';
    

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');
    
    public function scopeOrdenado($query)
    {
        return $query->orderBy('ordem', 'asc');
    }
    
    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeNotThis($query, $id)
    {
        return $query->where('id', '!=', $id);
    }
    
    public function cliente()
    {
        return $this->belongsTo('Clientes', 'clientes_id');
    }

    public function tipos()
    {
        return $this->belongsToMany('TiposTrabalho', 'tipo_trabalho', 'trabalho_id', 'tipo_id')
                    ->orderBy('titulo', 'asc');
    }

    public function itens()
    {
        return $this->belongsToMany('ItensTrabalho', 'item_trabalho', 'itens_id', 'trabalho_id')
                    ->orderBy('ordem', 'asc');
    }

    public function imagens()
    {
        return $this->hasMany('TrabalhoImagens', 'trabalhos_id')->ordenado();
    }

}
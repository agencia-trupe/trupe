<?php

class ApresentacaoImagens extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'apresentacao_imagens';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function apresentacao()
    {
    	return $this->belongsTo('Apresentacao', 'apresentacao_id');
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('ordem', 'asc');
    }
}
<?php

class TrabalheConoscoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$view = View::make('frontend.trabalhe-conosco.index');

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;
	}

	public function enviar()
	{
		$data['nome'] = Input::get('nome');
		$data['profissao'] = Input::get('profissao');
		$data['pretensao'] = Input::get('pretensao');
		$data['email'] = Input::get('email');
		$data['telefone'] = Input::get('telefone');
		$data['porque'] = Input::get('porque');

		if(Request::ajax()){
			$data['nome_arquivo'] = Input::get('nome_arquivo');
		}else{
			$curriculo = $this->upload();
			$data['nome_arquivo'] = $curriculo['filename'];
		}

		\Mail::send('emails.trabalhe-conosco', $data, function($message) use ($data)
		{
		    $message->to('contato@trupe.net', 'Trupe')
		    		->subject('Trabalhe Conosco')
		    		->replyTo($data['email'], $data['nome'])
		    		->attach(app_path('curriculos_recebidos/'.$data['nome_arquivo']));

		   	Log::info('Arquivo anexado :'.app_path('curriculos_recebidos/'.$data['nome_arquivo']));
		    if(Config::get('mail.pretend')) Log::info(View::make('emails.trabalhe-conosco', $data)->render());
		});

		if(!Request::ajax()){
			Session::flash('trabalheconoscoEnviado', true);
			return Redirect::route('trabalheconosco');
		}
	}

	public function upload()
	{
		$file = array('curriculo' => Input::file('curriculo'));
		$regras = array('curriculo' => 'required|mimes:doc,docx,pdf,txt,rtf,zip|max:4000');
		$uploadPath = app_path('curriculos_recebidos/');

		$validador = \Validator::make($file, $regras);

		$resposta = array(
			'erros' => false,
			'mensagem' => '',
			'filename' => ''
		);

		if($validador->fails()){
			$mensagens = $validador->messages();
			$resposta['erros'] = true;
			$resposta['mensagem'] = 'O Arquivo pode ser um doc, docx, pdf, txt ou rtf. E ter no máximo 4MB';
		}else{

			$filename_original = $file['curriculo']->getClientOriginalName();
			$filename_servidor = date('YmdHis').$filename_original;

		    $file['curriculo']->move($uploadPath, $filename_servidor);
		    $resposta['filename'] = $filename_original;
		    //$resposta['mime'] = $file['curriculo']->getMimeType();
		    //$resposta['size'] = $file['curriculo']->getSize();
		}
		return $resposta;
	}
}

<?php

class BrandingController extends BaseController{

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$view = View::make('frontend.branding.index');

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;		
	}

}
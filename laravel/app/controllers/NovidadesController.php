<?php

use \Novidades;

class NovidadesController extends BaseController{
	
	protected $layout = 'frontend.templates.index';

	public function index($slug = '')
	{
		if(!$slug){
			$detalhe = \Novidades::ordenado()->first();
		}else{
			$detalhe = \Novidades::slug($slug)->first();
		}

		if(is_null($detalhe)) App::abort('404');
		
		$listaNovidades = \Novidades::ordenado()->get();

		$view = View::make('frontend.novidades.index')->with(compact('detalhe'))
													  ->with(compact('listaNovidades'));

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;		
	}

}
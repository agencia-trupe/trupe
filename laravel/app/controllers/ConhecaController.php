<?php

use \Clientes;

class ConhecaController extends BaseController{

	protected $layout = 'frontend.templates.index';

	public function index($slug_area = '')
	{
		if($slug_area == '') $slug_area = 'perfil';
		if(!in_array($slug_area, array('perfil', 'clientes', 'equipe'))) App::abort('404');

		$listaClientes = Clientes::ordenado()->get();

		$view = View::make('frontend.conheca.'.$slug_area)->with(compact('slug_area'))
														  ->with(compact('listaClientes'));

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;
	}
}
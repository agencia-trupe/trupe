<?php

use \Apresentacao, \Clientes, \Trabalho, \TiposTrabalho;

class ProjetosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index($tipo_slug = '')
	{
		// Se não tiver filtro de Tipo, mostrar todos os projetos
		if(!$tipo_slug)
			$tipo_slug = 'todos';

		// Se não for clientes, nem todos, verificar se existe
		if($tipo_slug != 'clientes' && $tipo_slug != 'todos'){

			$tipo = TiposTrabalho::slug($tipo_slug)->first();
			
			if(!$tipo)
				$tipo_slug = 'todos'; // Se não existir, mostrar todos
			else
				$tipo_slug = $tipo->slug;
		}

		$listaTipos = TiposTrabalho::ordenado()->get();
		$listaClientes = Clientes::ordenado()->with('apresentacao')->get();
		$listaProjetos = Trabalho::with('cliente', 'tipos', 'itens')->ordenado()->get();
		$nroTipos = sizeof($listaTipos) + 2;

		$slugs_tipos = array();
		$classes = array();
		foreach ($listaProjetos as $key => $projeto) {
			// Criando a lista de slugs dos tipos para
			// comparar e esconder na view 
			// se não for igual ao filtro
			$slugs_tipos[$projeto->id] = $projeto->tipos->lists('slug');

			// Criando a lista de classes de acordo com
			// os tipos para filtrar pelo Isotopo na view
			if(sizeof($projeto->tipos)){
				
				foreach($projeto->tipos as $tp){
					$classes[$projeto->id][] = 'item-tipo-'.$tp->slug;
				}

				$classes[$projeto->id] = implode(' ', $classes[$projeto->id]);
			}else{
				$classes[$projeto->id] = '';
			}
			
				
		}

		$view = View::make('frontend.projetos.index')->with(compact('listaTipos'))
													 ->with(compact('nroTipos'))
												     ->with(compact('listaProjetos'))
												     ->with(compact('listaClientes'))
												     ->with(compact('slugs_tipos'))
												     ->with(compact('classes'))
												     ->with('filtro', $tipo_slug);
		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;
	}

	public function detalhes($slug_cliente = '', $slug = '')
	{
		if(!$slug_cliente || !$slug) App::abort('404');

		$trabalho = Trabalho::whereHas('cliente', function($q) use ($slug_cliente){
			$q->where('slug', '=', $slug_cliente);
		})->slug($slug)->first();

		if(!$trabalho) App::abort('404');

		$this->layout->with('css', 'css/projetos');
		$view = View::make('frontend.projetos.detalhes')->with(compact('trabalho'));

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;
	}
	
	public function apresentacao($slug_cliente = '')
	{
		if(!$slug_cliente) App::abort('404');

		$trabalho = Apresentacao::whereHas('cliente', function($q) use ($slug_cliente){
			$q->where('slug', '=', $slug_cliente);
		})->first();

		if(!$slug_cliente) App::abort('404');

		$this->layout->with('css', 'css/projetos');
		$view = View::make('frontend.projetos.detalhes')->with(compact('trabalho'));

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;
	}
}

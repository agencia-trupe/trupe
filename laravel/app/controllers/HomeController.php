<?php

use \Banners, \ChamadasClientes, \Atuacao, \Servicos, \Novidades;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$banners = Banners::ordenado()->get();
		$clientes = ChamadasClientes::ordenado()->get();
		$listaAtuacao = Atuacao::with('servicos')->ordenado()->get();
		$novidade = Novidades::ordenado()->first();
		$nro_servicos = sizeof(Servicos::all());

		$view = View::make('frontend.home.index')->with(compact('banners'))
												 ->with(compact('clientes'))
												 ->with(compact('nro_servicos'))
												 ->with(compact('listaAtuacao'))
												 ->with(compact('novidade'));

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;
	}

}

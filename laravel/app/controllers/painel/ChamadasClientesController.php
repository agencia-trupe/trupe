<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, ChamadasClientes;

class ChamadasClientesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '14';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$registros = ChamadasClientes::ordenado()->get();
		$this->layout->content = View::make('backend.chamadasclientes.index')->with(compact('registros'))
																			 ->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.chamadasclientes.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ChamadasClientes;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->link = Input::get('link');

		$icone = Thumb::makeIconeClientesPost('icone', 120, 80, 'clientes/home/', false, 'rgba(0,0,0,0)', false);
		if($icone) $object->icone = $icone;

		$icone_rosa = Thumb::makeIconeClientesPost('icone_rosa', 120, 80, 'clientes/home/rosa/', false, 'rgba(0,0,0,0)', false);
		if($icone_rosa) $object->icone_rosa = $icone_rosa;

		
		if($this->limiteInsercao && sizeof( ChamadasClientes::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Chamada criado com sucesso.');
			return Redirect::route('painel.chamadasclientes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Chamada!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.chamadasclientes.edit')->with('registro', ChamadasClientes::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ChamadasClientes::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->link = Input::get('link');

		$icone = Thumb::makeIconeClientesPost('icone', 120, 80, 'clientes/home/', false, 'rgba(0,0,0,0)', false);
		if($icone) $object->icone = $icone;

		$icone_rosa = Thumb::makeIconeClientesPost('icone_rosa', 120, 80, 'clientes/home/rosa/', false, 'rgba(0,0,0,0)', false);
		if($icone_rosa) $object->icone_rosa = $icone_rosa;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Chamada alterado com sucesso.');
			return Redirect::route('painel.chamadasclientes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Chamada!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ChamadasClientes::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Chamada removido com sucesso.');

		return Redirect::route('painel.chamadasclientes.index');
	}

}
<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Trabalho, \Clientes, \TiposTrabalho, \ItensTrabalho, \TrabalhoImagens;

class TrabalhoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Input::has('clientes_id')){
			$cliente = Clientes::with('trabalhos')->find(Input::get('clientes_id'));
			$registros = $cliente->trabalhos()->paginate(25);
		}else{ 
			$cliente = null;
			$registros = Trabalho::with('tipos')->ordenado()->paginate(2500);
		}
		
		$listaClientes = Clientes::ordenado()->get();
		$listaTipos = TiposTrabalho::ordenado()->get();
		$listaItens = ItensTrabalho::ordenado()->get();

		$this->layout->content = View::make('backend.trabalhos.index')->with(compact('registros'))
																	  ->with(compact('listaClientes'))
																	  ->with(compact('listaTipos'))
																	  ->with(compact('listaItens'))
																	  ->with(compact('cliente'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$listaClientes = Clientes::ordenado()->get();
		$listaTipos = TiposTrabalho::ordenado()->get();
		$listaItens = ItensTrabalho::ordenado()->get();

		$clientes_id = Input::has('clientes_id') ? Input::get('clientes_id') : null;

		$this->layout->content = View::make('backend.trabalhos.form')->with(compact('listaClientes'))
																	 ->with(compact('listaTipos'))
																	 ->with(compact('listaItens'))
																	 ->with(compact('clientes_id'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Trabalho;

		$object->clientes_id = Input::get('clientes_id');
		
		$capa = Thumb::make('capa', 650, 405, 'projetos/capas/', false, '#065562', false);
		if($capa) $object->capa = $capa;

		$thumb = Thumb::make('thumb', 200, 200, 'projetos/capas_redondas/');
		if($thumb) $object->thumb = $thumb;
		
		$object->titulo = Input::get('titulo');
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');
		$object->link = Input::get('link');
		$object->reel = Input::get('reel');

		$itens = Input::has('itens') ? Input::get('itens') : array();
		$tipos = Input::has('tipos') ? Input::get('tipos') : array();

		$object->ordem = -1;
		
		try {

			$object->save();

			$object->slug = $this->geraSlug($object->id);
			$object->save();

			$object->itens()->sync($itens);
			$object->tipos()->sync($tipos);

			// Salvar imagens do trabalho
			$imagens = Input::get('imagem');
			$descricoes = Input::get('descricao');

			if($imagens && is_array($imagens)){
				foreach ($imagens as $key => $value) {
					$obj = new TrabalhoImagens;
					$obj->trabalhos_id = $object->id;
					$obj->imagem = $value;
					$obj->ordem = $key;
					$obj->descricao = isset($descricoes[$key]) ? $descricoes[$key] : '';
					$obj->save();
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Trabalho criado com sucesso.');
			return Redirect::route('painel.trabalhos.index', array('clientes_id' => $object->clientes_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except(array('capa', 'thumb')));
			return Redirect::back()->withErrors(array('Erro ao criar Trabalho! <br>'.$e->getMessage()));

		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$registro = Trabalho::with('tipos', 'itens', 'cliente')->find($id);
		$listaClientes = Clientes::ordenado()->get();
		$listaTipos = TiposTrabalho::ordenado()->get();
		$listaItens = ItensTrabalho::ordenado()->get();

		$lista_ids_tipos_relacionados = $registro->tipos->lists('id');
		$lista_ids_itens_relacionados = $registro->itens->lists('id');
		$this->layout->content = View::make('backend.trabalhos.edit')->with(compact('registro'))
																	 ->with(compact('listaClientes'))
																	 ->with(compact('listaTipos'))
																	 ->with(compact('listaItens'))
																	 ->with(compact('lista_ids_tipos_relacionados'))
																	 ->with(compact('lista_ids_itens_relacionados'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Trabalho::find($id);

		$object->clientes_id = Input::get('clientes_id');
		
		$capa = Thumb::make('capa', 650, 405, 'projetos/capas/', false, '#065562', false);
		if($capa) $object->capa = $capa;

		$thumb = Thumb::make('thumb', 200, 200, 'projetos/capas_redondas/');
		if($thumb) $object->thumb = $thumb;

		$object->titulo = Input::get('titulo');
		$object->slug = $this->geraSlug($object->id);
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');
		$object->link = Input::get('link');
		$object->reel = Input::get('reel');

		$itens = Input::has('itens') ? Input::get('itens') : array();
		$tipos = Input::has('tipos') ? Input::get('tipos') : array();

		try {

			$object->save();

			$object->itens()->sync($itens);
			$object->tipos()->sync($tipos);

			TrabalhoImagens::where('trabalhos_id', '=', $object->id)->delete();
			
			$imagens = Input::get('imagem');
			$descricoes = Input::get('descricao');

			if($imagens && is_array($imagens)){
				foreach ($imagens as $key => $value) {
					$obj = new TrabalhoImagens;
					$obj->trabalhos_id = $object->id;
					$obj->imagem = $value;
					$obj->ordem = $key;
					$obj->descricao = isset($descricoes[$key]) ? $descricoes[$key] : '';
					$obj->save();
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Trabalho alterado com sucesso.');
			return Redirect::route('painel.trabalhos.index', array('clientes_id' => $object->clientes_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except(array('capa', 'thumb')));
			return Redirect::back()->withErrors(array('Erro ao criar Trabalho!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Trabalho::find($id);
		$clientes_id = $object->clientes_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Trabalho removido com sucesso.');

		return Redirect::route('painel.trabalhos.index', array('clientes_id' => $clientes_id));
	}

	/**
	 * Method from creating a unique slug for the project
	 * The url is composed by the client slug + project slug
	 * Examples : projetos/detalhes/meta-executivos/e-mail-marketing
	 *			  projetos/detalhes/meta-bpo/e-mail-marketing
	 *
	 * If we have the same project title whithin the same client
	 * we should append a incrementing integer
	 * Examples : projetos/detalhes/meta-executivos/e-mail-marketing
	 *			  projetos/detalhes/meta-executivos/e-mail-marketing-1
	 *			  projetos/detalhes/meta-executivos/e-mail-marketing-2
	 * And so on...
	 *
	 * @return String
	 */
	public function geraSlug($trabalho_atual_id)
	{
		$cliente_id 	= Input::get('clientes_id');
		$titulo  		= Input::get('titulo');
		$slug 			= Str::slug($titulo);
		$slug_check		= '';
		$append_digito 	= 1;
		$append_string  = '-';

		// Check for matching project slug and client id
		$query = Trabalho::whereHas('cliente', function($q) use ($cliente_id){
			$q->where('id', '=', $cliente_id);
		})->slug($slug)->notThis($trabalho_atual_id)->get();

		if(sizeof($query) == 0){
			// If there's no matching slugs for the same client,
			// just return the Slugified Title
			return $slug;
		}else{
			// While the current slug exists, do
			while(sizeof($query) > 0){
				
				// Append the digit to the current slug
				$slug_check = $slug . $append_string . $append_digito;
				
				// Check again
				$query = Trabalho::whereHas('cliente', function($q) use ($cliente_id){
					$q->where('id', '=', $cliente_id);
				})->slug($slug_check)->notThis($trabalho_atual_id)->get();

				// Increment the digit for the next iterarion
				$append_digito++;
			}
			// Return appended slug
			return $slug_check;
		}
	}
}
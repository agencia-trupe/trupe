<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Clientes, \Apresentacao;
use \TiposTrabalho, \ItensTrabalho, \ApresentacaoImagens;

class ApresentacaoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Redirect::route('painel.clientes.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$cliente = Clientes::find(Input::get('clientes_id'));

		if(!$cliente) return Redirect::back();

		$listaTipos = TiposTrabalho::ordenado()->get();
		$listaItens = ItensTrabalho::ordenado()->get();

		$this->layout->content = View::make('backend.apresentacao.form')->with(compact('cliente'))
																		->with(compact('listaTipos'))
																	  	->with(compact('listaItens'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Apresentacao;

		$object->clientes_id = Input::get('clientes_id');

		if(sizeof(Apresentacao::where('clientes_id', '=', $object->clientes_id)->get()) > 0)
			return Redirect::back()->withErrors(array('O Cliente já possui apresentação!'));

		$capa = Thumb::make('capa', 650, 405, 'projetos/capas/');
		if($capa) $object->capa = $capa;
		
		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');
		$object->link = Input::get('link');

		// Se todos os links de reel forem Vimeo, pegar Classe Vimeo do Projeto Marry Me
		$object->reel = Input::get('reel');
		
		try {

			$object->save();

			// Sincronizar itens do trabalho
			$itens = Input::has('itens') ? Input::get('itens') : array();
			$object->itens()->sync($itens);

			// Salvar imagens do trabalho
			$imagens = Input::get('imagem');
			$descricoes = Input::get('descricao');

			if($imagens && is_array($imagens)){
				foreach ($imagens as $key => $value) {
					$obj = new ApresentacaoImagens;
					$obj->apresentacao_id = $object->id;
					$obj->imagem = $value;
					$obj->ordem = $key;
					$obj->descricao = isset($descricoes[$key]) ? $descricoes[$key] : '';
					$obj->save();
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Apresentação criada com sucesso.');
			return Redirect::route('painel.clientes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Apresentação!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$listaTipos = TiposTrabalho::ordenado()->get();
		$listaItens = ItensTrabalho::ordenado()->get();

		$registro = Apresentacao::find($id);
		$cliente = $registro->cliente;

		$this->layout->content = View::make('backend.apresentacao.edit')->with(compact('registro'))
																		->with(compact('cliente'))
																		->with(compact('listaTipos'))
																	  	->with(compact('listaItens'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Apresentacao::find($id);

		$object->clientes_id = Input::get('clientes_id');

		$capa = Thumb::make('capa', 650, 405, 'projetos/capas/');
		if($capa) $object->capa = $capa;
		
		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');
		$object->link = Input::get('link');

		// Se todos os links de reel forem Vimeo, pegar Classe Vimeo do Projeto Marry Me
		$object->reel = Input::get('reel');

		try {

			$object->save();

			// Sincronizar itens do trabalho
			$itens = Input::has('itens') ? Input::get('itens') : array();
			$object->itens()->sync($itens);

			// Salvar imagens do trabalho
			$imagens = Input::get('imagem');
			$descricoes = Input::get('descricao');

			ApresentacaoImagens::where('apresentacao_id', '=', $object->id)->delete();

			if($imagens && is_array($imagens)){
				foreach ($imagens as $key => $value) {
					$obj = new ApresentacaoImagens;
					$obj->apresentacao_id = $object->id;
					$obj->imagem = $value;
					$obj->ordem = $key;
					$obj->descricao = isset($descricoes[$key]) ? $descricoes[$key] : '';
					$obj->save();
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Apresentação alterada com sucesso.');
			return Redirect::route('painel.apresentacao.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Apresentação!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Apresentacao::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Apresentação removida com sucesso.');

		return Redirect::route('painel.clientes.index');
	}

}
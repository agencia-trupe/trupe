<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \TiposTrabalho;

class TiposTrabalhoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.tipostrabalho.index')->with('registros', TiposTrabalho::ordenado()->paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.tipostrabalho.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new TiposTrabalho;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		if(sizeof(TiposTrabalho::slug($object->slug)->get()) > 0){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array("Já existe um cadastro para '<strong>{$object->titulo}</strong>'"));	
		}
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Tipo de Trabalho criado com sucesso.');
			return Redirect::route('painel.tipostrabalho.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Tipo de Trabalho!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.tipostrabalho.edit')->with('registro', TiposTrabalho::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = TiposTrabalho::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		if(sizeof(TiposTrabalho::slug($object->slug)->notThis($object->id)->get()) > 0){
			return Redirect::back()->withErrors(array("Já existe um cadastro para '<strong>{$object->titulo}</strong>'"));	
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Tipo de Trabalho alterado com sucesso.');
			return Redirect::route('painel.tipostrabalho.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Tipo de Trabalho!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = TiposTrabalho::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Tipo de Trabalho removido com sucesso.');

		return Redirect::route('painel.tipostrabalho.index');
	}

}
<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, ItensTrabalho;

class ItensTrabalhoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.itenstrabalho.index')->with('registros', ItensTrabalho::ordenado()->paginate(25));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.itenstrabalho.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ItensTrabalho;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		if(sizeof(ItensTrabalho::slug($object->slug)->get()) > 0){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array("Já existe um cadastro para '<strong>{$object->titulo}</strong>'"));	
		}

		$icone = Thumb::make('icone', null, null, 'itenstrabalho/');
		if($icone){
			if(!Thumb::checkSize('icone', 25, 25)){
				Session::flash('formulario', Input::except('icone'));
				return Redirect::back()->withErrors(array("O tamanho máximo para o ícone é '<strong>25px x 25px</strong>'"));	
			}
			$object->icone = $icone;	
		}

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Item criado com sucesso.');
			return Redirect::route('painel.itenstrabalho.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Item!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.itenstrabalho.edit')->with('registro', ItensTrabalho::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ItensTrabalho::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		if(sizeof(ItensTrabalho::slug($object->slug)->notThis($object->id)->get()) > 0){
			return Redirect::back()->withErrors(array("Já existe um cadastro para '<strong>{$object->titulo}</strong>'"));	
		}

		$icone = Thumb::make('icone', null, null, 'itenstrabalho/');
		if($icone){
			if(!Thumb::checkSize('icone', 25, 25)){
				Session::flash('formulario', Input::except('icone'));
				return Redirect::back()->withErrors(array("O tamanho máximo para o ícone é '<strong>25px x 25px</strong>'"));	
			}
			$object->icone = $icone;	
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Item alterado com sucesso.');
			return Redirect::route('painel.itenstrabalho.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Item!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ItensTrabalho::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Item removido com sucesso.');

		return Redirect::route('painel.itenstrabalho.index');
	}

}
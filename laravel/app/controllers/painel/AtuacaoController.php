<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Atuacao;

class AtuacaoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '8';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$registros = Atuacao::ordenado()->get();

		$this->layout->content = View::make('backend.atuacao.index')->with(compact('registros'))
																	->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.atuacao.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Atuacao;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->texto = Input::get('texto');

		$icone = Thumb::make('icone', null, null, 'atuacao/');
		if($icone) $object->icone = $icone;

		if($this->limiteInsercao && sizeof( Atuacao::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		if(sizeof(Atuacao::slug($object->slug)->get()) > 0){
			Session::flash('formulario', Input::except('icone'));
			return Redirect::back()->withErrors(array("Já existe um cadastro para '<strong>{$object->titulo}</strong>'"));	
		}
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Área de Atuação criada com sucesso.');
			return Redirect::route('painel.atuacao.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Área de Atuação!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.atuacao.edit')->with('registro', Atuacao::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Atuacao::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->texto = Input::get('texto');

		$icone = Thumb::make('icone', null, null, 'atuacao/');
		if($icone) $object->icone = $icone;

		if(sizeof(Atuacao::slug($object->slug)->notThis($object->id)->get()) > 0){
			Session::flash('formulario', Input::except('icone'));
			return Redirect::back()->withErrors(array("Já existe um cadastro para '<strong>{$object->titulo}</strong>'"));	
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Área de Atuação alterada com sucesso.');
			return Redirect::route('painel.atuacao.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Área de Atuação!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Atuacao::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Área de Atuação removida com sucesso.');

		return Redirect::route('painel.atuacao.index');
	}

}
<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, Novidades;

class NovidadesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.novidades.index')->with('registros', Novidades::ordenado()->paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.novidades.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Novidades;

		$object->titulo = Input::get('titulo');
		$object->chamada = Input::get('chamada');
		$object->texto = Input::get('texto');

		$imagem = Thumb::make('imagem', 500, null, 'novidades/', false, 'rgba(0, 0, 0, 0)');
		if($imagem) $object->imagem = $imagem;

		
		try {

			$object->save();

			$object->slug = $this->geraSlug($object->id);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Novidade criado com sucesso.');
			return Redirect::route('painel.novidades.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Novidade!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.novidades.edit')->with('registro', Novidades::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Novidades::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = $this->geraSlug($object->id);
		$object->chamada = Input::get('chamada');
		$object->texto = Input::get('texto');

		$imagem = Thumb::make('imagem', 500, null, 'novidades/', false, 'rgba(0, 0, 0, 0)');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Novidade alterado com sucesso.');
			return Redirect::route('painel.novidades.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Novidade!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Novidades::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Novidade removido com sucesso.');

		return Redirect::route('painel.novidades.index');
	}

	/**
	 * Method from creating a unique slug for the news
	 *
	 * @return String
	 */
	public function geraSlug($novidade_atual_id)
	{
		$titulo  		= Input::get('titulo');
		$slug 			= Str::slug($titulo);
		$slug_check		= '';
		$append_digito 	= 1;
		$append_string  = '-';

		$query = Novidades::slug($slug)->notThis($novidade_atual_id)->get();

		if(sizeof($query) == 0){
			return $slug;
		}else{
			while(sizeof($query) > 0){
				
				$slug_check = $slug . $append_string . $append_digito;
				
				$query = Novidades::slug($slug_check)->notThis($novidade_atual_id)->get();

				$append_digito++;
			}
			return $slug_check;
		}
	}
}
<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, Banners;

class BannersController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.banners.index')->with('registros', Banners::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.banners.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Banners;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->link = Input::get('link');

		$imagem_fundo = Thumb::make('imagem_fundo', 605, 605, 'banners/', false, 'rgba(0, 0, 0, 0)');
		if($imagem_fundo) $object->imagem_fundo = $imagem_fundo;

		$imagem_destaque = Thumb::make('imagem_destaque', 520, 310, 'banners/', false, 'rgba(0, 0, 0, 0)');
		if($imagem_destaque) $object->imagem_destaque = $imagem_destaque;
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Banner criado com sucesso.');
			return Redirect::route('painel.banners.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Banner!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.banners.edit')->with('registro', Banners::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Banners::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->link = Input::get('link');

		$imagem_fundo = Thumb::make('imagem_fundo', 605, 605, 'banners/', false, 'rgba(0, 0, 0, 0)');
		if($imagem_fundo) $object->imagem_fundo = $imagem_fundo;

		$imagem_destaque = Thumb::make('imagem_destaque', 520, 310, 'banners/', false, 'rgba(0, 0, 0, 0)');
		if($imagem_destaque) $object->imagem_destaque = $imagem_destaque;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Banner alterado com sucesso.');
			return Redirect::route('painel.banners.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Banner!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Banners::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Banner removido com sucesso.');

		return Redirect::route('painel.banners.index');
	}

}
<?php

namespace Painel;

// Core
use \Request, \Input, \DB;

// Models

// BaseClass
use \Controller;

class AjaxController extends Controller {

	public function __construct(){

	}

	/**
	 * Atualiza a ordem dos itens da tabela
	 *
	 * @return void
	 */
	function gravaOrdem(){
		/* Só aceita requisições feitas via AJAX */
		if(Request::ajax()){
			$menu = Input::get('data');
			$tabela = Input::get('tabela');

	        for ($i = 0; $i < count($menu); $i++) {
	        	DB::table($tabela)->where('id', $menu[$i])->update(array('ordem' => $i));
	        }
	        return json_encode($menu);
    	}else{
    		return "no-ajax";
    	}
	}

	/*
		Armazena as imagens dos Trabalhos e Apresentações
	*/
	public function upload()
	{
	 	$files = Input::file('files');
	    
        $assetPath = 'assets/images/projetos/redimensionadas/';
	    $originalPath = 'assets/images/projetos/originais/';
	    $thumbPath = 'assets/images/projetos/thumbs/';
	    $uploadPath = public_path($originalPath);
	    
	    $results = array();

	    foreach ($files as $file) {
	    	$filename = date('YmdHis').$file->getClientOriginalName();
	        $file->move($uploadPath, $filename);
	        $name = $originalPath.$filename;
	        $thumb = $thumbPath.$filename;

	       	\Thumb::makeFromFile($uploadPath, $filename, 1500, null, public_path($assetPath), '#FFFFFF', false);
	       	\Thumb::makeFromFile($uploadPath, $filename, 300, 300, public_path($thumbPath), '#FFFFFF', true);

	       	$results[] = array(
	        	'thumb' => $thumb,
	        	'filename' => $filename
	        );	        
	    }

	    return array('files' => $results);		
	}	
}

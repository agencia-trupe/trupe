<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Servicos, \Atuacao;

class ServicosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$atuacao_id = Input::get('atuacao_id');

		if($atuacao_id){
			$atuacao = Atuacao::with('servicos')->find($atuacao_id);
			$registros = $atuacao->servicos;
		}else{
			return Redirect::route('painel.atuacao.index');
		}
		
		$listaAtuacao = Atuacao::ordenado()->get();

		$this->layout->content = View::make('backend.servicos.index')->with(compact('registros'))
																	 ->with(compact('listaAtuacao'))
																	 ->with(compact('atuacao'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$atuacao = Atuacao::find(Input::get('atuacao_id'));
		if(!$atuacao) return Redirect::route('painel.atuacao.index');

		$listaClientes = \Clientes::ordenado()->get();
		$this->layout->content = View::make('backend.servicos.form')->with(compact('atuacao'))
																	->with(compact('listaClientes'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Servicos;

		$object->atuacao_id = Input::get('atuacao_id');

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->texto = Input::get('texto');

		if(sizeof(Servicos::slug($object->slug)->get()) > 0){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array("Já existe um cadastro para '<strong>{$object->titulo}</strong>'"));	
		}
		
		try {

			$object->save();

			//relacionados
			$relacionados = Input::has('relacionados') ? Input::get('relacionados') : array();
			$object->relacionados()->sync($relacionados);

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Serviço criado com sucesso.');
			return Redirect::route('painel.servicos.index', array('atuacao_id' => $object->atuacao_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Serviço!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$listaClientes = \Clientes::ordenado()->get();
		$registro = Servicos::with('relacionados')->find($id);
		$id_projetos_relacionados = $registro->relacionados->lists('id');
		
		$this->layout->content = View::make('backend.servicos.edit')->with(compact('registro'))
																	->with(compact('id_projetos_relacionados'))
																	->with(compact('listaClientes'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Servicos::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->texto = Input::get('texto');

		if(sizeof(Servicos::slug($object->slug)->notThis($object->id)->get()) > 0){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array("Já existe um cadastro para '<strong>{$object->titulo}</strong>'"));	
		}

		try {

			$object->save();

			//relacionados
			$relacionados = Input::has('relacionados') ? Input::get('relacionados') : array();
			$object->relacionados()->sync($relacionados);

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Serviço alterado com sucesso.');
			return Redirect::route('painel.servicos.index', array('atuacao_id' => $object->atuacao_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Serviço!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Servicos::find($id);
		$atuacao_id = $object->atuacao_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Serviço removido com sucesso.');

		return Redirect::route('painel.servicos.index', array('atuacao_id' => $atuacao_id));
	}

}
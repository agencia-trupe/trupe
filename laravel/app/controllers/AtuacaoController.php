<?php

use \Atuacao, \Servicos, \Trabalho;

class AtuacaoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index($area_slug = '')
	{
		$listaAtuacao = Atuacao::with('servicos')->ordenado()->get();

		if($area_slug){
			$atuacaoSelecionada = Atuacao::with('servicos')->slug($area_slug)->first();

			if(!$atuacaoSelecionada) $atuacaoSelecionada = null;

		}else{
			$atuacaoSelecionada = null;
		}

		$view = View::make('frontend.atuacao.index')->with(compact('listaAtuacao'))
													->with(compact('atuacaoSelecionada'));

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;
	}

	public function detalhes($area_slug = '', $servico_slug = '')
	{
		if(!$area_slug || !$servico_slug) return Redirect::route('atuacao');

		$listaAtuacao = Atuacao::with('servicos')->ordenado()->get();
		$servicoSelecionado = Servicos::with('relacionados.itens', 'relacionados.cliente')->slug($servico_slug)->first();

		if(!$servicoSelecionado) return Redirect::route('atuacao');

		//$projetos = Trabalho::

		$view = View::make('frontend.atuacao.detalhes')->with(compact('listaAtuacao'))
													   ->with(compact('servicoSelecionado'));
		
		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;
	}
}

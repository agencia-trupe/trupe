<?php

class CriacaoDeSitesController extends BaseController{

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$view = View::make('frontend.criacao-de-sites.index');

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;		
	}

}
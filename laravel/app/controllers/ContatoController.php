<?php

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$view = View::make('frontend.contato.index');

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;
	}

	public function enviar()
	{
		$data['nome'] = Input::get('nome');
		$data['email'] = Input::get('email');
		$data['telefone'] = Input::get('telefone');
		$data['mensagem'] = Input::get('mensagem');

		\Mail::send('emails.contato', $data, function($message) use ($data)
		{
		    $message->to('contato@trupe.net', 'Trupe')
		    		->subject('Contato')
		    		->replyTo($data['email'], $data['nome']);

		    if(Config::get('mail.pretend')) Log::info(View::make('emails.contato', $data)->render());
		});

		if(!Request::ajax()){
			Session::flash('contatoEnviado', true);
			return Redirect::route('contato');
		}
	}
}

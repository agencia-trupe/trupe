<?php

class ComunicacaoController extends BaseController{

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$view = View::make('frontend.comunicacao.index');

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;		
	}

}
<?php

// Route::get('fix', function(){
// 	$clientes = \Clientes::all();
// 	foreach ($clientes as $key => $cliente) {
// 		$img = \Thumb::makeIconeClientesFile(public_path('assets/images/clientes/originais/'), $cliente->imagem, 120, 80, public_path('assets/images/clientes/home/'), 'rgba(0,0,0,0)', false);
// 		$cliente->icone = $img ? $img : '';
// 		$cliente->save();
// 	}
// });

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('projetos/{tipo_slug?}', array('as' => 'projetos', 'uses' => 'ProjetosController@index'));
Route::get('projetos/detalhes/{slug_cliente?}/{slug?}', array('as' => 'projetos.detalhes', 'uses' => 'ProjetosController@detalhes'));
Route::get('projetos/apresentacao/{slug?}', array('as' => 'projetos.detalhes', 'uses' => 'ProjetosController@apresentacao'));
Route::get('areas-de-atuacao/{area_slug?}', array('as' => 'atuacao', 'uses' => 'AtuacaoController@index'));
Route::get('areas-de-atuacao/{area_slug?}/{servico_slug?}', array('as' => 'atuacao.detalhes', 'uses' => 'AtuacaoController@detalhes'));
Route::get('trabalhe-conosco', array('as' => 'trabalheconosco', 'uses' => 'TrabalheConoscoController@index'));
Route::post('trabalhe-conosco', array('as' => 'trabalheconosco.enviar', 'uses' => 'TrabalheConoscoController@enviar'));
Route::post('trabalhe-conosco/upload', array('as' => 'trabalheconosco.upload', 'uses' => 'TrabalheConoscoController@upload'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::post('contato', array('as' => 'contato.enviar', 'uses' => 'ContatoController@enviar'));
Route::get('conheca-a-trupe/{area?}', array('as' => 'conheca', 'uses' => 'ConhecaController@index'));
Route::get('criacao-de-sites', array('as' => 'criacaodesites', 'uses' => 'CriacaoDeSitesController@index'));
Route::get('comunicacao-e-design', array('as' => 'comunicacaoedesign', 'uses' => 'ComunicacaoController@index'));
Route::get('branding', array('as' => 'branding', 'uses' => 'BrandingController@index'));
Route::get('novidades/{slug?}', array('as' => 'novidades', 'uses' => 'NovidadesController@index'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
	Route::any('multi-upload', 	array('as' => 'painel.portfolioimagens.upload', 'uses' => 'Painel\AjaxController@upload'));
	Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('clientes', 'Painel\ClientesController');
	Route::resource('trabalhos', 'Painel\TrabalhoController');
	Route::resource('tipostrabalho', 'Painel\TiposTrabalhoController');
	Route::resource('itenstrabalho', 'Painel\ItensTrabalhoController');
	Route::resource('apresentacao', 'Painel\ApresentacaoController');
	Route::resource('atuacao', 'Painel\AtuacaoController');
	Route::resource('servicos', 'Painel\ServicosController');
Route::resource('banners', 'Painel\BannersController');
Route::resource('chamadasclientes', 'Painel\ChamadasClientesController');
Route::resource('novidades', 'Painel\NovidadesController');
//NOVASROTASDOPAINEL//
});
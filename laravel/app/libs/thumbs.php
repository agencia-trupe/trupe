<?php

use \Image, \Input;

/**
*	Classe wrapper para criação de Thumbs
*	@author Bruno Monteiro
*	@since 10/04/2014
*/
class Thumb
{
	/* Vou receber o nome do input
	*  pegar o input via post
	*  checar dimensões
	*  redimensionar de acordo
	*  retornar o nome da imagem ou false
	*/

	/* MAKE FROM POST */
	public static function make($input = false, $largura = 0, $altura = 0, $destino = '', $customTimestamp = false, $bgcolor = '#FFFFFF', $cortarExcedente = true)
	{
		if(!$input || !$destino) return false;

		if(Input::hasFile($input)){

			$imagem = Input::file($input);
			$filename = ($customTimestamp !== false) ? $customTimestamp.$imagem->getClientOriginalName() : date('YmdHis').$imagem->getClientOriginalName();
			
			ini_set('memory_limit','256M');
			$imgobj = Image::make(Input::file($input)->getRealPath());

			if($altura == null && $largura == null){
				$imgobj->save('assets/images/'.$destino.$filename, 100);
			}elseif($altura == null || $largura == null){
  				$imgobj->resize($largura, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->save('assets/images/'.$destino.$filename, 100);
  			}else{

				$ratio = $imgobj->width() / $imgobj->height();
	  			$nratio = $largura / $altura;

	  			if($cortarExcedente){
					if ($ratio > $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save('assets/images/'.$destino.$filename, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save('assets/images/'.$destino.$filename, 100);
					}
				}else{
					if ($ratio < $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save('assets/images/'.$destino.$filename, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save('assets/images/'.$destino.$filename, 100);
					}
				}
			}
			$imgobj->destroy();
			return $filename;
		}else{
			return false;
		}
	}

	/* MAKE FROM FILE */
	public static function makeFromFile($dir = '', $arquivo = false, $largura = 0, $altura = 0, $destino = '', $bgcolor = '#FFFFFF', $cortarExcedente = true)
	{
		if(!$dir || !$arquivo || !$destino) return false;

		if(file_exists($dir.$arquivo)){

			$imgobj = Image::make(file_get_contents($dir.$arquivo));

  			if($altura == null || $largura == null){
  				$imgobj->resize($largura, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->save($destino.$arquivo, 100);
  			}else{

				$ratio = $imgobj->width() / $imgobj->height();
	  			$nratio = $largura / $altura;

	  			if($cortarExcedente){
					if ($ratio > $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save($destino.$arquivo, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save($destino.$arquivo, 100);
					}
				}else{
					if ($ratio < $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save($destino.$arquivo, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save($destino.$arquivo, 100);
					}
				}
			}
			$imgobj->destroy();
			return $arquivo;
		}else{
			return false;
		}
	}

	public static function saveOriginal($input = false, $destino = '', $customTimestamp = false)
	{
		if(!$input || !$destino) return false;

		if(Input::hasFile($input)){

			$imagem = Input::file($input);
			$filename = ($customTimestamp !== false) ? $customTimestamp.$imagem->getClientOriginalName() : date('YmdHis').$imagem->getClientOriginalName();
			
			ini_set('memory_limit','256M');
			$imgobj = Image::make(Input::file($input)->getRealPath());

			$imgobj->save('assets/images/'.$destino.$filename, 100);
			$imgobj->destroy();
			return $filename;
		}else{
			return false;
		}
	}

	public static function checkSize($input, $max_x = 0, $max_y = 0)
	{
		if(!$input) return false;

		if(Input::hasFile($input)){
			
			ini_set('memory_limit','256M');
			$imgobj = Image::make(Input::file($input)->getRealPath());
			
			if(($imgobj->width() > $max_x) || ($imgobj->height() > $max_y)){
				return false;
			}else{
				return true;
			}

		}else{
			return false;
		}
	}

	/* MAKE FROM POST */
	public static function makeThumbClientes($input = false, $largura = 0, $altura = 0, $destino = '', $customTimestamp = false, $bgcolor = '#FFFFFF', $cortarExcedente = true)
	{
		// 170,70
		if(!$input || !$destino) return false;

		if(Input::hasFile($input)){

			$imagem = Input::file($input);
			$filename = ($customTimestamp !== false) ? $customTimestamp.$imagem->getClientOriginalName() : date('YmdHis').$imagem->getClientOriginalName();
			
			ini_set('memory_limit','256M');
			$imgobj = Image::make(Input::file($input)->getRealPath());

			$ratio = $imgobj->width() / $imgobj->height();
  			$nratio = $largura / $altura;

			if ($ratio < $nratio) {
				// crop height
				$imgobj->resize(null, 70, function ($constraint) {
												    $constraint->aspectRatio();
												    $constraint->upsize();
												})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
												  ->save('assets/images/'.$destino.$filename, 100);
			} else {
				// crop width
				$imgobj->resize(170, null, function ($constraint) {
												    $constraint->aspectRatio();
												    $constraint->upsize();
												})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
												  ->save('assets/images/'.$destino.$filename, 100);
			}

			$imgobj->destroy();
			return $filename;
		}else{
			return false;
		}
	}

	/* MAKE FROM FILE */
	public static function makeIconeClientesFile($dir = '', $arquivo = false, $largura = 0, $altura = 0, $destino = '', $bgcolor = '#FFFFFF', $cortarExcedente = true)
	{
		if(!$dir || !$arquivo || !$destino) return false;

		if(strpos($arquivo, '.jpg') !== -1){
			$arquivo_png = str_ireplace('.jpg', '.png', $arquivo);
		}else{
			$arquivo_png = $arquivo;
		}

		if(file_exists($dir.$arquivo)){

			$imgobj = Image::make(file_get_contents($dir.$arquivo));

  			if($altura == null || $largura == null){
  				$imgobj->resize($largura, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->save($destino.$arquivo_png, 100);
  			}else{

				$ratio = $imgobj->width() / $imgobj->height();
	  			$nratio = $largura / $altura;

	  			if($cortarExcedente){
					if ($ratio > $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save($destino.$arquivo_png, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save($destino.$arquivo_png, 100);
					}
				}else{
					if ($ratio < $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save($destino.$arquivo_png, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save($destino.$arquivo_png, 100);
					}
				}
			}
			$imgobj->destroy();
			return $arquivo_png;
		}else{
			return false;
		}
	}	

	/* MAKE FROM POST */
	public static function makeIconeClientesPost($input = false, $largura = 0, $altura = 0, $destino = '', $customTimestamp = false, $bgcolor = '#FFFFFF', $cortarExcedente = true)
	{
		if(!$input || !$destino) return false;

		if(Input::hasFile($input)){

			$imagem = Input::file($input);
			$filename = ($customTimestamp !== false) ? $customTimestamp.$imagem->getClientOriginalName() : date('YmdHis').$imagem->getClientOriginalName();
			
			if(strpos($filename, '.jpg') !== -1){
				$filename_png = str_ireplace('.jpg', '.png', $filename);
			}else{
				$filename_png = $filename;
			}

			ini_set('memory_limit','256M');
			$imgobj = Image::make(Input::file($input)->getRealPath());

			if($altura == null && $largura == null){
				$imgobj->save('assets/images/'.$destino.$filename, 100);
			}elseif($altura == null || $largura == null){
  				$imgobj->resize($largura, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->save('assets/images/'.$destino.$filename_png, 100);
  			}else{

				$ratio = $imgobj->width() / $imgobj->height();
	  			$nratio = $largura / $altura;

	  			if($cortarExcedente){
					if ($ratio > $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save('assets/images/'.$destino.$filename_png, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save('assets/images/'.$destino.$filename_png, 100);
					}
				}else{
					if ($ratio < $nratio) {
						// crop height
						$imgobj->resize(null, $altura, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save('assets/images/'.$destino.$filename_png, 100);
					} else {
						// crop width
						$imgobj->resize($largura, null, function ($constraint) {
														    $constraint->aspectRatio();
														    $constraint->upsize();
														})->resizeCanvas($largura, $altura, 'center', false, $bgcolor)
														  ->save('assets/images/'.$destino.$filename_png, 100);
					}
				}
			}
			$imgobj->destroy();
			return $filename_png;
		}else{
			return false;
		}
	}		
}

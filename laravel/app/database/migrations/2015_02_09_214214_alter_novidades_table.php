<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNovidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('novidades', function(Blueprint $table)
		{
			$table->string('titulo_home')->after('titulo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('novidades', function(Blueprint $table)
		{
			$table->dropColumn('titulo_home');
		});
	}

}

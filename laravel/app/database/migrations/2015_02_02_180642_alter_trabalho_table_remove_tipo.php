<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTrabalhoTableRemoveTipo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('trabalhos', function(Blueprint $table)
		{
			$table->dropColumn('tipos_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('trabalhos', function(Blueprint $table)
		{
			$table->dropColumn('tipos_id')->after('clientes_id');			
		});
	}

}

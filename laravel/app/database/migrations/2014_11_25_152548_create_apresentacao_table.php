<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApresentacaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('apresentacao', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('clientes_id');
			$table->string('capa');
			$table->string('titulo');
			$table->string('slug');
			$table->text('olho');
			$table->text('texto');
			$table->string('link');
			$table->string('reel');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('apresentacao');
	}

}

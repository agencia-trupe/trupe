<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
	  
class CreateChamadasClientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chamadas_clientes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->string('texto');
			$table->string('link');
			$table->string('icone');
			$table->string('icone_rosa');
			$table->integer('ordem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('chamadas_clientes');
	}

}

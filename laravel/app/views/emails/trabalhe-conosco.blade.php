<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Qual é o seu nome? </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Qual é a sua profissão ou formação? </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $profissao }}</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Qual é a sua pretensão salarial? </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $pretensao }}</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Qual é o seu e-mail? </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Qual é o seu telefone? </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Fale um pouco sobre porque devemos te contratar: </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $porque }}</span><br>
</body>
</html>
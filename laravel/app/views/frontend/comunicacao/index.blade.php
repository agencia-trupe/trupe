@section('conteudo')

	<div class="centralizar titulo">
		<h1>COMUNICAÇÃO E DESIGN</h1>
	</div>

	<div class="faixa-amarela">
		<div class="centralizar">
			<div class="row">
				<div class="columns hide-for-small-only medium-4 large-5 xlarge-5">
					<figure>
						<img src="assets/images/comunicacao/img-comunicacao-integrada.png" alt="Comunicação integrada - Branding, impressos, sinalização, propaganda.">
						<figcaption>Comunicação integrada &middot; Branding, impressos, sinalização, propaganda.</figcaption>
					</figure>
				</div>
				<div class="columns small-12 medium-8 large-7 xlarge-7">
					<section>
						<p>
							Comunicar é falar. E todos notamos a diferença quando alguém fala bem. Aprimore o discurso que sua empresa faz para o mercado com um bom plano de comunicação e ações interligadas de marca, comunicação, design e experiência digital.
						</p>
						<p>
							Como fazer isso? Fale com a Trupe. A equipe de criação e planejamento trará muitas ideias para alinhar todos os pontos de contato com o cliente com uma mensagem única e direcionada. Integrada mesmo. Online + offline.
						</p>

						<h2>
							FORTE SOLUÇÃO DE COMUNICAÇÃO INTEGRADA <br>
							COM DESIGN ESTRATÉGICO + EXPERIÊNCIA DIGITAL
						</h2>

						<figure>
							<img src="assets/images/comunicacao/comunicacao-design.png" alt="Design, Estratégia, Comunicação">
						</figure>
						
					</section>
				</div>
			</div>
		</div>
	</div>

	<div class="centralizar lista-areas-comunicacao">
		<div class="row" data-equalizer>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/comunicacao/icone-plano-comunicacao.png" alt="PLANO DE COMUNICAÇÃO">
				</figure>
				<h2>
					PLANO DE COMUNICAÇÃO
				</h2>
				<p>
					Com planejamento o investimento em comunicação dá mais resultados e a empresa tem objetivos claros para atingir. Com um bom plano de comunicação o cliente percebe claramente o discurso da empresa, e com esse conhecimento prévio é muito mais fácil fechar uma venda. Deixe a comunicação divulgar seu trabalho e poupe tempo de prospecção e venda na hora de fechar um negócio. Faça toda a comunicação da sua empresa com a Trupe.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/comunicacao/icone-materiais-impressos.png" alt="MATERIAIS IMPRESSOS">
				</figure>
				<h2>
					MATERIAIS IMPRESSOS
				</h2>
				<p>
					Mesmo numa era digital como a nossa a comunicação impressa continua sendo indispensável. Como suporte da estratégia digital ou vice-versa. Se alinhadas, todas as peças juntas ganham força e marcam na mente do cliente. Com a Trupe, seu folder, panfleto, catálogo, pasta, cartão, folheto, flyer, mala direta e o que mais você precisar, estará em linha com a marca e o site sempre, e será marcante no processo de vendas.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/comunicacao/icone-publicacoes.png" alt="PUBLICAÇÕES">
				</figure>
				<h2>
					PUBLICAÇÕES
				</h2>
				<p>
					Se a sua empresa precisa comunicar ‘muito’ ou o seu produto é uma publicação - jornal, revista, anuário, livro, etc - você pode fazer isso com a Trupe. Além do excelente design e gerenciamento de todas as etapas até a produção gráfica, você pode contar com a integração da peça impressa com meios digitais, versão online e promoção on e offline.
				</p>
			</div>		
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/comunicacao/icone-embalagens.png" alt="EMBALAGENS">
				</figure>
				<h2>
					EMBALAGENS
				</h2>
				<p>
					Nada é melhor do que comunicar na hora certa. E quando mais um cliente iria querer saber sobre seu produto se não na hora da compra? Embalagens pensadas para comunicar ajudam neste momento crucial. Além de informações obrigatórias você faz propaganda e... vende! E não são só produtos finais que precisam de embalagens bem pensadas. Seu delivery ou relatório anual podem ser sempre muito bem embalados!
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/comunicacao/icone-brindes.png" alt="BRINDES">
				</figure>
				<h2>
					BRINDES
				</h2>
				<p>
					Brindes fazem sua empresa ganhar mais um momento de lembrança da marca toda vez que são utilizados, e principalmente na hora em que são recebidos. A Trupe cria brindes que fazem parte da estratégia de comunicação da empresa, criativos e memoráveis. E ainda pensa e planeja as formas de entrega para tornar o momento um acontecimento que marque ainda mais o perfil da sua marca na mente do cliente.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/comunicacao/icone-sinalizacao.png" alt="SINALIZAÇÃO">
				</figure>
				<h2>
					SINALIZAÇÃO
				</h2>
				<p>
					O melhor meio de divulgar a marca numa empresa é expô-la de maneira correta na própria empresa. E que tal ir além da exposição e explorar toda a identidade visual na hora de ‘decorar’ o ambiente? O processo de sinalização é mais uma oportunidade de deixar a marca falar. E neste momento a marca fala para todos os públicos, interno e externo. Conte com a Trupe para seus projetos de sinalização, ambientação e envelopamento de frota.
				</p>
			</div>		
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/comunicacao/icone-solucoes-eventos.png" alt="SOLUÇÕES PARA EVENTOS">
				</figure>
				<h2>
					SOLUÇÕES PARA EVENTOS
				</h2>
				<p>
					Um evento precisa de comunicação em todas as etapas. Desde a criação da ‘cara’ do evento com o desenvolvimento da marca e identidade visual, passando pela promoção e divulgação até a sinalização do ambiente e ações de pós-evento. A Trupe cuida de todos esses pontos e ainda entende muito de regras, leis e outros aspectos que envolvem eventos comerciais. E sua empresa ainda contará com comunicação digital integrada no mesmo pacote.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/comunicacao/icone-propaganda.png" alt="PROPAGANDA">
				</figure>
				<h2>
					PROPAGANDA
				</h2>
				<p>
					Quando a propaganda é contínua e focada, os negócios ficam mais fáceis. Se seus possíveis clientes já tiverem ouvido falar da sua empresa e estiverem com uma boa sensação sobre seus produtos e sua reputação antes mesmo de você fazer o primeiro contato, você terá chances  muito maiores de fechar uma venda. Então faça propaganda. E faça propaganda com um plano de comunicação integrada com a Trupe!
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/comunicacao/icone-promocao.png" alt="PROMOÇÃO">
				</figure>
				<h2>
					PROMOÇÃO
				</h2>
				<p>
					Promover é vender. Pra isso você pode utilizar as mais diversas formas, ferramentas e ajudas possíveis. Essa é a hora da inovação e da criatividade. É só não esquecer a estratégia e o planejamento, que as ações de promoção sempre terão bons resultados. E são tantas as opções que não dá pra citar aqui. Promova sua empresa, sua marca, seu produto e seu site sempre e conte com a ajuda da equipe especialista da Trupe. 
				</p>
			</div>
		</div>
		
		<div class="link-final">
			<a href="projetos/website" title="CONFIRA ALGUNS CASES DE CLIENTES QUE FAZEM SUA COMUNICAÇÃO COM A TRUPE">CONFIRA ALGUNS CASES DE CLIENTES QUE FAZEM SUA COMUNICAÇÃO COM A TRUPE &raquo;</a>
		</div>		
	</div>

@stop
@section('conteudo')
	
	<div class="centralizar titulo">
		<h1>ATUAÇÃO &middot; DESIGN & WEB</h1>
	</div>	

	<div class="conteudo-servicos">
		<div class="centralizar">
			<aside>
				<ul>
					@if(sizeof($listaAtuacao))
						@foreach ($listaAtuacao as $atuacao)
							<li><h2>{{ $atuacao->titulo }}</h2></li>
							@if(sizeof($atuacao->servicos))
								@foreach($atuacao->servicos as $servico)
									<li><a href="areas-de-atuacao/{{ $atuacao->slug }}/{{ $servico->slug }}" @if($servico->slug == $servicoSelecionado->slug) class="ativo" @endif title="{{ $servico->titulo }}" data-slug="{{ $servico->slug }}">&raquo; {{ $servico->titulo }}</a></li>
								@endforeach
							@endif
						@endforeach
					@endif
				</ul>
			</aside>
			<section>
				<div class="hide-for-large-up btn-voltar-atuacao">
					<a href="areas-de-atuacao" title="Menu Atuação"><img src="assets/images/layout/seta-menuprojetos.png" alt="Voltar para o menu de atuação">MENU ATUAÇÃO</a>
				</div>
				<div class="detalhe">
					<h1>{{ $servicoSelecionado->titulo }}</h1>
					<div class="texto">
						{{ $servicoSelecionado->texto }}
					</div>
					@if(sizeof($servicoSelecionado->relacionados) > 0)
					<div class="relacionados">
						<h2>Veja alguns exemplos de <span>{{ $servicoSelecionado->atuacao->titulo }}:</span></h2>
						<div id="lista-relacao">
							@foreach($servicoSelecionado->relacionados as $projeto)
								<a href="projetos/detalhes/{{ $projeto->cliente->slug }}/{{ $projeto->slug }}" title="{{ $projeto->titulo }}" class="item-portfolio thumb-redonda relacao">
									<div class="imagem"><img src="assets/images/projetos/capas_redondas/{{ $projeto->thumb }}"></div>
									<div class="capa">
										<h1>{{ $projeto->cliente->titulo }}</h1>
										<h2>{{ $projeto->titulo }}</h2>
										<p>{{ $projeto->olho }}</p>
										@if(sizeof($projeto->itens) > 0)
											<ul class="links">
											@foreach ($projeto->itens as $item)
												<li title="{{ $item->titulo }}"><img src="assets/images/itenstrabalho/{{ $item->icone }}" alt="{{ $item->titulo }}"></li>
											@endforeach
											</ul>
										@endif
									</div>
								</a>
							@endforeach
						</div>
					</div>
					@endif
				</div>
				<div class="outras-areas">
					<h2>Além de <span>{{ $servicoSelecionado->atuacao->titulo }}</span> a Trupe atua com:</h2>
					<div class="lista-outras">
						@if(sizeof($listaAtuacao))
							@foreach($listaAtuacao as $atuacao)
								@if($atuacao->id != $servicoSelecionado->atuacao->id)
									<a href="areas-de-atuacao/{{ $atuacao->slug }}" title="{{ $atuacao->titulo }}">
										<img src="assets/images/atuacao/{{ $atuacao->icone }}" alt="{{ $atuacao->titulo }}">
										<span>{{ $atuacao->titulo }}</span>
									</a>
								@endif
							@endforeach
						@endif
					</div>
				</div>
				<div class="hide-for-large-up btn-voltar-atuacao">
					<a href="areas-de-atuacao" title="Menu Atuação"><img src="assets/images/layout/seta-menuprojetos.png" alt="Voltar para o menu de atuação">MENU ATUAÇÃO</a>
				</div>
			</section>
		</div>
	</div>

@stop
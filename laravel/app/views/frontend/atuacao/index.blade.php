@section('conteudo')
	
	<div class="centralizar titulo">
		<h1>ATUAÇÃO &middot; DESIGN & WEB</h1>
	</div>

	<div class="faixa-roxa">
		<div class="centralizar">
			<h1>
				Criamos peças funcionais e diferenciadas,<br>
				e ainda por cima bem bonitas!
			</h1>
			<h2>
				Design Estratégico &middot; Criamos com profissionalismo, mas também adoramos experimentar, inovar e ir além.<br>
				Oferecemos as melhores soluções em design impresso e digital, e excelente usabilidade na criação de sites.
			</h2>

			<ul class="lista-areas">
				@if(sizeof($listaAtuacao))
					@foreach ($listaAtuacao as $atuacao)
						<li @if($atuacaoSelecionada && $atuacao->id == $atuacaoSelecionada->id) class="aberto" @endif>
							<a href="areas-de-atuacao/{{ $atuacao->slug }}" title="{{ $atuacao->titulo }}" data-slug="{{ $atuacao->slug }}" class="icone-area">
								<img src="assets/images/atuacao/{{ $atuacao->icone }}" alt="{{ $atuacao->titulo }}">
								<span>{{ $atuacao->titulo }}</span>
							</a>
							<div class="texto-atuacao">
								<div class="texto">
									{{ $atuacao->texto }}
								</div>
								<div class="links">
									@if(sizeof($atuacao->servicos))
										<ul class="lista-servicos">
										@foreach ($atuacao->servicos as $servico)
											<li><a href="areas-de-atuacao/{{ $atuacao->slug }}/{{ $servico->slug }}" title="{{ $servico->titulo }}" class="link-servico">&raquo; {{ $servico->titulo }}</a></li>
										@endforeach
										</ul>
									@endif
								</div>
							</div>
						</li>
					@endforeach
				@endif
			</ul>
		</div>
	</div>

	<div class="centralizar ilustracao">
		<img src="assets/images/layout/img-ilustra-atuacao.png" alt="Trupe - Áreas de Atuação">
	</div>

@stop
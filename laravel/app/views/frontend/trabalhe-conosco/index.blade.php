@section('conteudo')

	<div class="centralizar trabalhe-conosco">
		<div class="titulo">
			<h1>TRABALHE CONOSCO</h1>
		</div>
		<section>
			<aside>
				<h1>
					Se você é bom no que faz<br>
					e quer fazer parte de<br>
					uma equipe eficiente e<br>
					que aprende sempre,<br>
					manda seu currículo pra cá!
				</h1>
				<h2>
					Não esquece de citar jobs que já realizou<br>
					ou colocar o link do seu portfólio.
				</h2>
				
				<div class="contem-link-cv" style="display:none;">				
					<img src="assets/images/layout/anexo-trabalheConosco.png" id="envie-cv" alt="Envie seu currículo!">				
				</div>
			</aside>
			<div class="form">
				<h2>CADASTRE-SE EM NOSSO BANCO DE CURRÍCULOS</h2>
				<form id="formTrabalheConosco" action="trabalhe-conosco" method="POST" autocomplete="off" enctype="multipart/form-data"  @if(Session::has('trabalheconoscoEnviado')) class="no-js-form-enviado" @endif>
					<div class="form-interno">
						<ol class="perguntas">
							<li id="q1" data-proximo="2">
								<span><label for="q1-input">Qual é o seu nome?</label></span>
								<div class="input">
									<input id="q1-input" name="nome" type="text" required data-mensagem="Informe seu nome!">
								</div>
							</li>
							<li id="q2" data-proximo="3">
								<span><label for="q2-input">Qual é a sua profissão ou formação?</label></span>
								<div class="input">
									<input id="q2-input" name="profissao" type="text" required data-mensagem="Informe sua Profissão atual ou Formação!">
								</div>
							</li>
							<li id="q3" data-proximo="4">
								<span><label for="q3-input">Qual é a sua pretensão salarial?</label></span>
								<div class="input">
									<input id="q3-input" name="pretensao" type="text" required data-mensagem="E a sua pretensão salarial?">
								</div>
							</li>
							<li id="q4" data-proximo="5">
								<span><label for="q4-input">Qual é o seu e-mail?</label></span>
								<div class="input">
									<input id="q4-input" name="email" type="email" required data-mensagem="Informe seu e-mail!">
								</div>
							</li>
							<li id="q5" data-proximo="6">
								<span><label for="q5-input">Qual é o seu telefone?</label></span>
								<div class="input">
									<input id="q5-input" name="telefone" type="text" required data-mensagem="Qual é seu telefone?">
								</div>
							</li>
							<li id="q6" data-proximo="fim">
								<span><label for="q6-input">Fale um pouco sobre porque devemos te contratar:</label></span>
								<div class="input">
									<input id="q6-input" name="porque" type="text" required data-mensagem="Fale um pouco por que devemos te contratar!">
								</div>
							</li>
						</ol>
						<div class="controles">
							<button class="next"></button>
							<div class="progresso">
								<div class="passo" id="passo-1"></div>
								<div class="passo" id="passo-2"></div>
								<div class="passo" id="passo-3"></div>
								<div class="passo" id="passo-4"></div>
								<div class="passo" id="passo-5"></div>
								<div class="passo" id="passo-6"></div>
							</div>
							<span class="mensagem-erro"></span>
							<span class="navegacao">
								<div><button title="1&ordf; questão" data-voltar="1"></button></div>
								<div><button title="2&ordf; questão" data-voltar="2"></button></div>
								<div><button title="3&ordf; questão" data-voltar="3"></button></div>
								<div><button title="4&ordf; questão" data-voltar="4"></button></div>
								<div><button title="5&ordf; questão" data-voltar="5"></button></div>
								<div><button title="6&ordf; questão" data-voltar="6"></button></div>
							</span>
						</div>
					</div>
					<div class="segunda-parte">
						<div class="anexar">
							<label for="fileupload">ANEXAR CURRÍCULO</label>
							<input type="file" id="fileupload" name="curriculo" data-url="{{ URL::route('trabalheconosco.upload') }}" required>
							<input type="hidden" name="nome_arquivo" value="" id="nomeArquivo">
						</div>
						<div class="mensagem-erro-arquivo"></div>
						<input class="submit" type="submit" value="Enviar">
					</div>
				</form>
				<div id="resposta-form" @if(Session::has('trabalheconoscoEnviado')) class="no-js-form-enviado" @endif>
					Obrigado! Seu currículo foi enviado.<br> Entraremos em contato assim que possível!
				</div>
			</div>
		</section>

	</div>

@stop
<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <meta name="keywords" content="" />

    <title>Trupe</title>
    <meta name="description" content="">
    <meta property="og:title" content="Trupe"/>
    <meta property="og:description" content=""/>

    <meta property="og:site_name" content="Trupe"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array('css/build'))?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>	
	
	<?=Assets::JS(array('vendor/modernizr/modernizr'))?>

	<script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
		// Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-', '');
		// ga('send', 'pageview');
	</script>
</head>
<body id="trupe-html-body">

	<header @if(str_is('home', Route::currentRouteName())) class="home" @endif>
		@include('frontend.partials.faixa-topo')
		@include('frontend.partials.menu')
	</header>

	<div id="conteudo-principal">
		<div id="conteudo-swap">
			@yield('conteudo')
		</div>
	</div>
	
	<footer>
		<div class="centralizar">
			<div class="row" data-equalizer>
				<div class="columns small-6 medium-4 large-3 xlarge-3" data-equalizer-watch>
					<div class="footer-pad">						
						<h2>CONTATO</h2>
						<p>
							(11) 2861 1590<br>
							Rua Coronel Diogo 762<br>
							Aclimação - São Paulo, SP<br>
						</p>

						<div class="assinatura show-for-small-only" id="assinatura-mobile">
							&copy; {{Date('Y')}} TRUPE AGÊNCIA CRIATIVA<br>
							TODOS OS DIREITOS RESERVADOS<br>
							<img src="assets/images/layout/rodape-marca-trupe.png" alt="TRUPE AGÊNCIA CRIATIVA">
						</div>

						<h3>FALE CONOSCO</h3>
						<form action="contato" method="post" id="footer-contato">
							<input type="text" name="nome" placeholder="nome" id="footer-contato-nome" required>
							<input type="email" name="email" placeholder="email" id="footer-contato-email" required>
							<input type="text" name="telefone" placeholder="telefone" id="footer-contato-telefone">
							<textarea name="mensagem" placeholder="mensagem" id="footer-contato-mensagem" required></textarea>
							<input type="submit" value="ENVIAR &raquo;">
						</form>
						<div id="resposta-footer-form">
							Obrigado!<br> Sua mensagem foi enviada com sucesso!<br> Entraremos em contato assim que possível!
						</div>
					</div>
				</div>
				<div class="columns small-6 medium-8 large-7 xlarge-7" data-equalizer-watch>
					<div class="row">
						<div class="columns small-12 medium-6 large-7 xlarge-7" data-equalizer-watch>
							<div class="footer-pad">
								<h2>SERVIÇOS E SOLUÇÕES</h2>
								<ul>
									<li><a href='projetos' title='nossos projetos'>&raquo; nossos projetos</a></li>
									<li><a href='criacao-de-sites' title='criação de sites'>&raquo; criação de sites</a></li>
									<li><a href='comunicacao-e-design' title='comunicação e design'>&raquo; comunicação e design</a></li>
									<li><a href='branding' title='branding'>&raquo; branding</a></li>
								</ul>
								<ul>
									<li><a href='areas-de-atuacao/criacao-e-design' title='criação & design'>&raquo; criação & design</a></li>
									<li><a href='areas-de-atuacao/branding' title='branding & sinalização'>&raquo; branding & sinalização</a></li>
									<li><a href='areas-de-atuacao/midia-impressa' title='mídia impressa'>&raquo; mídia impressa</a></li>
									<li><a href='areas-de-atuacao/websites-e-hotsites' title='websites e hotsites'>&raquo; websites e hotsites</a></li>
									<li><a href='areas-de-atuacao/sistemas-online' title='sistemas online'>&raquo; sistemas online</a></li>
									<li><a href='areas-de-atuacao/redes-sociais' title='redes sociais'>&raquo; redes sociais</a></li>
									<li><a href='areas-de-atuacao/estrategia-e-comunicacao-digital' title='estratégia e comunicação digital'>&raquo; estratégia e comunicação digital</a></li>
									<li><a href='areas-de-atuacao/consultoria' title='consultoria'>&raquo; consultoria</a></li>							
								</ul>
							</div>
						</div>
						<div class="columns small-12 medium-6 large-5 xlarge-5" data-equalizer-watch>
							<div class="footer-pad mobile-reduzido">
								<h2>A TRUPE</h2>
								<ul>
									<li><a href='conheca-a-trupe' title='conheça a trupe'>&raquo; conheça a trupe</a></li>
									<li><a href='conheca-a-trupe/equipe' title='equipe'>&raquo; equipe</a></li>
									<li><a href='conheca-a-trupe/clientes' title='clientes'>&raquo; clientes</a></li>
									<li><a href='areas-de-atuacao' title='atuação: design & web'>&raquo; atuação: design & web</a></li>
									<li><a href='novidades' title='novidades'>&raquo; novidades</a></li>
								</ul>
								<ul>
									<li><a href='trabalhe-conosco' title='trabalhe conosco'>&raquo; trabalhe conosco</a></li>
									<li><a href='contato' title='contato'>&raquo; contato</a></li>
								</ul>

								<div class="assinatura show-for-medium-only" id="assinatura-medium">
									&copy; {{Date('Y')}} TRUPE AGÊNCIA CRIATIVA<br>
									TODOS OS DIREITOS RESERVADOS<br>
									<img src="assets/images/layout/rodape-marca-trupe.png" alt="TRUPE AGÊNCIA CRIATIVA">
								</div>
							</div>
						</div>			
					</div>		
				</div>
				<div class="columns hide-for-small-only hide-for-medium-only large-2 xlarge-2" data-equalizer-watch>
					<div class="footer-pad">
						<div class="assinatura">
							&copy; {{Date('Y')}} TRUPE AGÊNCIA CRIATIVA<br>
							TODOS OS DIREITOS RESERVADOS<br>
							<img src="assets/images/layout/rodape-marca-trupe.png" alt="TRUPE AGÊNCIA CRIATIVA">
						</div>
					</div>
				</div>
			</div>			
		</div>		
	</footer>

	<?=Assets::JS(array('js/main'))?>

</body>
</html>

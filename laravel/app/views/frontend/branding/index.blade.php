@section('conteudo')

	<div class="centralizar titulo">
		<h1>BRANDING</h1>
	</div>
	<div class="faixa-amarela-escuro">
		<div class="centralizar">
			<div class="row">
				<div class="columns hide-for-small-only medium-4 large-5 xlarge-5">
					<figure>
						<img src="assets/images/branding/img-branding-gestaomarca.png" alt="Trupe Branding">
						<figcaption>Branding &middot; Gestão de Marca</figcaption>
					</figure>
				</div>
				<div class="columns small-12 medium-8 large-7 xlarge-7">
					<section>
						<p>
							Branding é a construção e o fortalecimento de uma marca.
						</p>
						<p>
							A Trupe desenvolveu uma metodologia própria para seus processos de branding, focada em criar uma conexão entre marca e consumidor e proporcionar maior fidelidade do público. O trabalho consiste em analisar o perfil da empresa e do público alvo para identificar e diagnosticar o melhor caminho para construir uma marca forte e eficaz.
						</p>

						<h2>
							GESTÃO DE MARCA É TUDO O QUE A TRUPE FAZ
						</h2>

						<figure>
							<img src="assets/images/branding/branding-gestao-marca.png" alt="Branding e Gestão de Marca">
						</figure>
						
					</section>
				</div>
			</div>
		</div>
	</div>
	<div class="centralizar branding">
		<h2>
			CONFIRA ALGUMAS MARCAS CRIADAS PELA TRUPE <br>
			E ALGUMAS PARA AS QUAIS DESENVOLVEMOS PROJETOS DE BRANDING:
		</h2>
		<ul class="listaBranding small-block-grid-2 medium-block-grid-4 large-block-grid-5 xlarge-block-grid-5">
			<li>
				<a href="projetos/apresentacao/amanco" title="Amanco" class="marca-cliente">
					<img src="assets/images/branding/amanco.png"
						 alt="Amanco">
				</a>
			</li>
			<li>
				<a href="projetos/apresentacao/amanco" title="Amanco" class="marca-cliente">
					<img src="assets/images/branding/amanco.png"
						 alt="Amanco">
				</a>
			</li>
			<li>
				<a href="projetos/apresentacao/amanco" title="Amanco" class="marca-cliente">
					<img src="assets/images/branding/amanco.png"
						 alt="Amanco">
				</a>
			</li>
			<li>
				<a href="projetos/apresentacao/amanco" title="Amanco" class="marca-cliente">
					<img src="assets/images/branding/amanco.png"
						 alt="Amanco">
				</a>
			</li>
			<li>
				<a href="projetos/apresentacao/amanco" title="Amanco" class="marca-cliente">
					<img src="assets/images/branding/amanco.png"
						 alt="Amanco">
				</a>
			</li>
			<li>
				<a href="projetos/apresentacao/amanco" title="Amanco" class="marca-cliente">
					<img src="assets/images/branding/amanco.png"
						 alt="Amanco">
				</a>
			</li>
		</div>
	</div>

@stop
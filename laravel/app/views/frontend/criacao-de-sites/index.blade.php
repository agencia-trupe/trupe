@section('conteudo')

	<div class="centralizar titulo">
		<h1>CRIAÇÃO DE SITES</h1>
	</div>

	<div class="faixa-turquesa">
		<div class="centralizar">
			<div class="row">
				<div class="columns hide-for-small-only medium-4 large-5 xlarge-5">
					<figure>
						<img src="assets/images/criacao/img-criacaodesites.png" alt="Criação de sites responsivos - Otimização e SEO">
						<figcaption>Criação de sites responsivos &middot; Otimização e SEO</figcaption>
					</figure>
				</div>
				<div class="columns small-12 medium-8 large-7 xlarge-7">
					<section>
						<p>
							Sua empresa não precisa de 'alguém para fazer o site'. Sua empresa precisa que todo o marketing comunique bem para vender mais e melhor. Onde todas as peças de comunicação tenham a mesma linguagem, inclusive o site.
						</p>

						<h2>
							A CRIAÇÃO DE SITES NA TRUPE É UM PROCESSO QUE <br>
							ENVOLVE PROFISSIONAIS DE DIFERENTES ESPECIALIDADES
						</h2>

						<figure>
							<img src="assets/images/criacao/design+tecnologia.png" alt="Design + Tecnologia = Trupe">
						</figure>

						<p>
							A Trupe atua na criação de sites com foco na usabilidade e sempre com interfaces bonitas e simples, fáceis de navegar, com excelente design e bem construídas. Utilizamos as tecnologias mais recentes e indicamos a melhor solução para cada projeto, sempre considerando os objetivos do cliente. O nosso processo de criação de sites foca a união do design com a tecnologia desde a consultoria inicial, o que garante sites leves, bonitos e de perfeito funcionamento.
						</p>

						<h2>
							DESENVOLVIMENTO WEB NO PONTO ONDE DESIGN E<br> TECNOLOGIA SE ENCONTRAM
						</h2>
					</section>
				</div>
			</div>
		</div>
	</div>

	<div class="centralizar lista-areas">
		<div class="row" data-equalizer>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-responsive.png" alt="RESPONSIVE DESIGN">
				</figure>
				<h2>
					RESPONSIVE DESIGN
				</h2>
				<p>
					Você quer que seu site seja acessado em tablets e smartphones? Então você precisa de responsive design. Sites responsivos são sites que se adaptam a qualquer dispositivo e a qualquer resolução de tela sem perder conteúdo ou dificultar sua visualização. A responsividade em um site permite que um único código de programação seja perfeitamente visualizado em computadores de mesa, notebooks, tablets ou smartphones.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-usabilidade.png" alt="USABILIDADE">
				</figure>
				<h2>
					USABILIDADE
				</h2>
				<p>
					Usabilidade tem a ver com eficiência. Um site com boa usabilidade permite que qualquer pessoa, com qualquer nível de conhecimento sobre o site, encontre facilmente o que quer. Boa usabilidade é sinônimo de boa arquitetura da informação. Pensar a usabilidade é uma das primeiras ações da Trupe no planejamento para criar um site. Organizamos informações de maneira lógica e criamos ferramentas adequadas para cada projeto.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-navegabilidade.png" alt="NAVEGABILIDADE">
				</figure>
				<h2>
					NAVEGABILIDADE
				</h2>
				<p>
					Navegabilidade é promover e facilitar o acesso à informação. A criação de menus e o correto agrupamento de botões e cliques facilitam o uso do site e garantem que o usuário encontre a informação da maneira mais rápida e lógica possível. Planejando a navegabilidade dos sites a Trupe propõe a estrutura de menus, os termos, botões, ferramentas e links para tornar o site simples para quem o utiliza.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-seo.png" alt="SEO - OTIMIZAÇÃO DE CONTEÚDO">
				</figure>
				<h2>
					SEO &middot; OTIMIZAÇÃO DE CONTEÚDO
				</h2>
				<p>
					Otimizar um site (SEO) é garantir que os buscadores - Google na maioria das vezes - leiam corretamente o conteúdo e o classifiquem para exibição nos resultados orgânicos. A otimização é um conjunto de práticas que facilitam a leitura automática dos sistemas de busca para melhor posicionamento do seu site nos resultados. Todos os sites desenvolvidos pela Trupe seguem boas práticas de SEO.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-cms.png" alt="CMS - GERENCIADOR DE CONTEÚDO">
				</figure>
				<h2>
					CMS &middot; GERENCIADOR<br> DE CONTEÚDO
				</h2>
				<p>
					Sistemas gerenciadores de conteúdo (CMS) são ferramentas essenciais para quem faz um site hoje em dia. O CMS permite que o próprio cliente faça atualização de conteúdos sem precisar da agência sempre que quiser alterar texto, imagem ou vídeo do seu próprio site. Os gerenciadores de conteúdo desenvolvidos pela Trupe são intuitivos e muito fáceis de usar.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-redessociais.png" alt="COMPARTILHAMENTO EM REDES SOCIAIS">
				</figure>
				<h2>
					COMPARTILHAMENTO<br> EM REDES SOCIAIS
				</h2>
				<p>
					Promover seu site através das redes sociais é primordial atualmente. Porém, alguns sites não estão corretamente preparados para aparecerem bem no compartilhamento e a promoção acaba virando propaganda negativa, exibindo marcas cortadas ou imagens incompletas. Para que um site seja compartilhado sem erros diversos cuidados devem ser tomados na hora de programar. A Trupe toma todos esses cuidados.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-php-javascript.png" alt="PHP & JAVASCRIPT">
				</figure>
				<h2>
					PHP & JAVASCRIPT
				</h2>
				<p>
					Sites dinâmicos precisam de uma linguagem de sistemas para serem construídos. A Trupe desenvolve todos os seus sistemas internamente e utiliza PHP para garantir preço competitivo e alta produtividade. Para a melhor experiência do usuário também utilizamos Javascript que traz agilidade e efeitos amigáveis à navegação. Todo o código dos sites é desenvolvido pela Trupe sem a utilização de templates ou códigos prontos.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-html5-css3.png" alt="HTML & CSS">
				</figure>
				<h2>
					HTML & CSS
				</h2>
				<p>
					HTML e CSS são a ‘linguagem do browser’. Com eles é possível ter sites com visualização perfeita em computadores, tablets, smartphones e até outros dispositivos que ainda irão surgir. A Trupe cria seus sites com os mais recentes recursos de HTML e CSS, inclusive HTML 5. A equipe especializada sabe como aproveitar os melhores recursos e ainda indica o que e como usar em cada projeto.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-design-interface.png" alt="INTERFACE & DESIGN">
				</figure>
				<h2>
					INTERFACE & DESIGN
				</h2>
				<p>
					Um site bonito apenas não vende. Um site deve ser bonito, mas deve também informar, vender e atender a expectativa do usuário. Interfaces bem construídas criam ferramentas que facilitam a vida do cliente. Por isso o design é importante na concepção do projeto inicial. Por isso a Trupe foca no design e na tecnologia com a mesma importância. Construímos sites bonitos, comerciais e funcionais.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-performance.png" alt="PERFORMANCE">
				</figure>
				<h2>
					PERFORMANCE
				</h2>
				<p>
					Um dos fatores que a Trupe considerou na hora de escolher as tecnologias com que trabalha - PHP, MySQL, Javascript, HTML e CSS - foi garantir alta performance dos sites que cria. Além desse cuidado a Trupe aplica técnicas no desenvolvimento de sites que fazem sites ‘rápidos’ e sempre seguros. Seus desenvolvedores também sabem tudo sobre a plataforma Linux e resolvem qualquer parada com os servidores.
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-google-analytics.png" alt="GOOGLE ANALYTICS">
				</figure>
				<h2>
					GOOGLE ANALYTICS
				</h2>
				<p>
					Depois de finalizado o site é hora de acompanhar seu desempenho e sua evolução. Para isso o Google Analytics disponibiliza ferramentas gratuitas com relatórios sobre acessos, tecnologias utilizadas, origens do tráfego para o site, etc. Todos os sites desenvolvidos pela Trupe contam com o cadastro no Google Analytics. Se o cliente esquecer de fazê-lo a Trupe não esquece!
				</p>
			</div>
			<div class="contem-area columns small-12 medium-6 large-4 xlarge-4" data-equalizer-watch>
				<figure>
					<img src="assets/images/criacao/icone-relatorios.png" alt="RELATÓRIOS">
				</figure>
				<h2>
					RELATÓRIOS
				</h2>
				<p>
					Alguns websites geram informações para análise. E algumas vezes é complicado ter acesso a elas. A Trupe desenvolve módulos de relatórios consolidados em seus CMS com recursos personalizados para cada situação. Desde uma simples listagem dos contatos recebidos ao cruzamento de informações de diversas origens, os sistemas da Trupe fornecem relatórios simples de serem extraídos e sempre muito inteligentes.
				</p>
			</div>
		</div>

		<div class="link-final">
			<a href="projetos/website" title="Confira os projetos de criação de sites em nosso portfólio de trabalhos">CONFIRA OS PROJETOS DE CRIAÇÃO DE SITES EM NOSSO PORTFÓLIO DE TRABALHOS &raquo;</a>
		</div>

		<div class="row collapse contem-destaques">
			<div class="columns small-12 medium-12 large-6 xlarge-6">
				<div class="destaque esquerda">
					<div class="row collapse">
						<div class="columns small-12 medium-3 large-4 xlarge-4">
							<figure>
								<img src="assets/images/criacao/icone-lojas-virtuais.png" alt="LOJAS VIRTUAIS">
							</figure>
						</div>
						<div class="columns small-12 medium-9 large-8 xlarge-8">
							<h2>LOJAS VIRTUAIS</h2>
							<div class="paragrafos">
								<p>
									A Trupe cria lojas virtuais totalmente personalizadas. Todo o desenvolvimento é feito especificamente para o cliente, sem a utilização de módulos prontos que limitam a criação de novas funcionalidades e o crescimento do projeto no futuro. Cada detalhe da loja é especialmente desenhado com a cara da sua marca, e com o jeito que você quer vender.
								</p>
								<p class="strong">
									Não importa se o seu jeito de vender ainda não existe, a Trupe ajuda a criar o e-commerce que vende como você quer.
								</p>
								<p>
									E o pagamento pode ser direto com cartão de crédito, boleto, transferência bancária ou ainda utilizando gateways de pagamento como o PagSeguro e o Itaú Shopline. Você é quem decide!
								</p>
							</div>
							<div class="link-destaque">
								<a href="projetos/loja-virtual" title="CONFIRA ALGUMAS LOJAS VIRTUAIS">CONFIRA ALGUMAS LOJAS VIRTUAIS »</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="columns small-12 medium-12 large-6 xlarge-6">
				<div class="destaque direita">
					<div class="row collapse">
						<div class="columns small-12 medium-3 large-4 xlarge-4">
							<figure>
								<img src="assets/images/criacao/icone-sistemas-web.png" alt="SISTEMAS ONLINE - WEB">
							</figure>
						</div>
						<div class="columns small-12 medium-9 large-8 xlarge-8">
							<h2>SISTEMAS ONLINE &middot; WEB</h2>
							<div class="paragrafos">
								<p>
									Quando um site precisa ser mais que um site, quando ele precisa se tornar um produto ou serviço completo, é preciso desenvolver um sistema na web, totalmente online. Nem sempre empresas que fazem websites são desenvolvedoras de sistemas online, mas a Trupe não é uma dessas empresas. A Trupe conta com uma equipe que reúne especialistas em design de serviços, levantamento de requisitos, análise de sistemas, desenvolvimento de interfaces e código apropriado para criar sistemas online de alta performance, criativos e funcionais.
								</p>
								<p class="strong">
									Se você tem uma ideia a Trupe tem o poder de transformá-la em um projeto web de sucesso.
								</p>
							</div>
							<div class="link-destaque">
								<a href="projetos/sistemas-online" title="CONFIRA ALGUNS SISTEMAS ONLINE">CONFIRA ALGUNS SISTEMAS ONLINE »</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>

@stop
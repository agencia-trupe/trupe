@section('conteudo')
	
	<div class="faixaBanners bg-turquesa-escuro">

		<div class="centralizar">
			
			<div id="banners">
				@if(sizeof($banners))
					@foreach($banners as $banner)
						<div class="banner">
							<div class="imagem-fundo">
								<img src="assets/images/banners/{{ $banner->imagem_fundo }}" alt="{{ $banner->titulo }}">
							</div>
							<a href="{{ $banner->link }}" title="{{ $banner->titulo }}" class="imagem-frente">
								<img src="assets/images/banners/{{ $banner->imagem_destaque }}" alt="{{ $banner->titulo }}">
							</a>
							<a href="{{ $banner->link }}" title="{{ $banner->titulo }}" class="texto">
								<h1>{{ $banner->titulo }}</h1>
								<p>{{ $banner->texto }}</p>
								<div class="ver-projeto">ver projeto &raquo;</div>
							</a>
						</div>
					@endforeach
				@endif
			</div>

		</div>
		
		<div id="bannersControl">
			<div class="centralizar"></div>
		</div>

	</div>
	
	<div class="chamadas-home">

		<div class="centralizar">
			<h1 id="titulo-chamada-home">
				<span>
					MAIS QUE IDEIAS
				</span>
			</h1>
		</div>

	 	<ul class="small-block-grid-1 medium-block-grid-1 large-block-grid-3 xlarge-block-grid-5">
			<li>
				<div class="chamada">
					<h2>CONSULTORIA <br> E PLANEJAMENTO</h2>
					<p>
						Somos especialistas em marketing e tecnologia. Criamos conceitos, definimos o que e como deve ser comunicado. Planejamos a execução completa. E também desenvolvemos tudo que estiver no planejamento.
					</p>
				</div>
			</li>
			<li>
				<div class="chamada">
					<h2>COMUNICAÇÃO <br> INTEGRADA</h2>
					<p>
						A mensagem a ser transmitida é sempre única, por isso o site tem que dizer o mesmo que a marca, o folder, o catálogo ou a rede social. É isso o que a trupe faz com a comunicação da sua empresa.
					</p>
				</div>
			</li>
			<li>
				<div class="chamada">
					<h2>CRIAÇÃO, AÇÃO E <br> PRESENÇA DIGITAL</h2>
					<p>
						Criamos sites completos desde a concepção, design até a programação. Mantemos sua empresa ativa na internet com campanhas digitais, banners, redes sociais, blogs, lojas virtuais, aplicativos e mais!
					</p>
				</div>
			</li>
			<li>
				<div class="chamada">
					<h2>CRIAÇÃO E <br> DESIGN GRÁFICO</h2>
					<p>
						Marca, folder, pasta, panfleto, apresentação institucional, etiqueta, envelope, catálogo, mostruário, revista, jornal, mala-direta, anúncio... Mesmo no atual mundo digital ainda se faz necessário material impresso de apoio à venda.
					</p>
				</div>
			</li>
			<li>
				<div class="chamada">
					<h2>DESENVOLVIMENTO WEB <br> E SISTEMAS ONLINE</h2>
					<p>
						A Trupe realmente desenvolve seus produtos internamente, sem terceirização. Ter um site pode não ser o bastante se o que sua empresa precisa é de um sistema online completo para atender os anseios dos clientes. A gente faz!
					</p>
				</div>
			</li>
		</ul>

	</div>

	<div class="clientes-home">
		<ul class="small-block-grid-3 medium-block-grid-4 large-block-grid-5 xlarge-block-grid-7">
			@if(sizeof($clientes))
				@foreach ($clientes as $cliente)
					<li>
						<a href="{{ $cliente->link }}"
											title="{{ $cliente->titulo }}"
											style="background-image:url('assets/images/clientes/home/rosa/{{ $cliente->icone_rosa }}')" 
											data-image-hover="url('assets/images/clientes/home/{{ $cliente->icone }}')"
											data-image="url('assets/images/clientes/home/rosa/{{ $cliente->icone_rosa }}')">
							<div class='chamada_cliente' style="background-image:url('assets/images/clientes/home/{{ $cliente->icone }}')">
								{{ nl2br($cliente->texto) }}
							</div>
						</a>
					</li>		
				@endforeach
			@endif
		</ul>
	</div>

	<div class="main-home">
		<div class="centralizar">
			<div class="row collapse">
				<div class="columns hide-for-small-only medium-12 large-12 xlarge-5">
					<aside id="lista-servicos">
						<ul>
						<?php $c = 0 ?>
						@if(sizeof($listaAtuacao))
							@foreach ($listaAtuacao as $atuacao)
								@if(sizeof($atuacao->servicos))
									@foreach($atuacao->servicos as $servico)
										<li><a href="areas-de-atuacao/{{ $atuacao->slug }}/{{ $servico->slug }}" title="{{ $servico->titulo }}">&raquo; {{ $servico->titulo }}</a></li>
										<?php $c++ ?>
										@if(($c) == $nro_servicos/2)
											</ul><ul>
										@endif
									@endforeach
								@endif
							@endforeach
						@endif
						</ul>
					</aside>
				</div>
				<div class="columns small-12 medium-12 large-12 xlarge-7">
					<section>
						<p>
							A Trupe é uma agência de criação para mídias impressas e digitais.
						</p>
						<h2>
							Adoramos fazer peças interativas inteligentes e simples, que são boas de ver e ótimas pra usar.
						</h2>
						<p>
							Atuamos com design e mídias digitais de forma personalizada, desenvolvemos projetos completos para cada cliente, tecnicamente bem construídos e alinhados a toda a comunicação da empresa.
						</p>
						<p>
							E estamos fazendo isso desde 2001!
						</p>

						<div class="row collapse cards-home fechados">

							<div class="columns small-12 medium-6 large-6 xlarge-6">

								<div class="card-3">
									<img src="assets/images/layout/ilustra-design+tecnologia.png" alt="Design & Tecnologia">
								</div>

								<div class="card-4">
									<a href="projetos/apresentacao/projeto-hydros" title="Projeto Hydros">
										<h1>CASES</h1>
										<div class="imagem">
											<img src="assets/images/layout/cases-marcaExemplo.png" alt="Projeto Hydros">											
										</div>
									</a>
								</div>

							</div>

							<div class="columns small-12 medium-6 large-6 xlarge-6">

								<div class="card-1">
									<h1>SAINDO DO FORNO</h1>
									<div class="links">
										<span title="COLLABORATION PROJECT MEXICHEM">COLLABORATION PROJECT MEXICHEM</span>
									</div>
								</div>

								<div class="card-2">
									@if(isset($novidade->chamada))
										<a href="novidades/{{ $novidade->slug }}" title="{{ $novidade->titulo }}">
											<h1>Novidades</h1>
											<p>
												{{ $novidade->chamada }}
											</p>
										</a>
									@else
										
									@endif
								</div>

								<div class="card-5">
									<a href="trabalhe-conosco" title="QUER FAZER PARTE DA NOSSA EQUIPE?">QUER<br>FAZER PARTE<br>DA NOSSA<br>EQUIPE?</a>
								</div>

							</div>							

						</div>
					</section>
				</div>
			</div>
		</div>
	</div>

@stop
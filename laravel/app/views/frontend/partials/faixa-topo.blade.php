<div class="faixaTopo bg-petroleo">
	<div class="centralizar">
		criamos e desenvolvemos o que sua empresa precisa:
		<ul id="linksTopo">
			@if(sizeof($listaServicosTopo))
				@foreach($listaServicosTopo as $servicoTopo)
					<li><a href="areas-de-atuacao/{{ $servicoTopo->atuacao->slug }}/{{ $servicoTopo->slug }}" title="{{ $servicoTopo->titulo }}" data-clone="{{ mb_strtoupper($servicoTopo->titulo) }} &laquo;">{{ mb_strtoupper($servicoTopo->titulo) }} &laquo;</a></li>
				@endforeach
			@endif
		</ul>
	</div>
</div>
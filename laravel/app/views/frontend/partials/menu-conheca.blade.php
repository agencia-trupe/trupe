<div class="titulo">
	<h1>CONHEÇA A TRUPE</h1>
</div>
<nav id="navegacao-conheca">
	<ul>
		<li><a href="conheca-a-trupe/perfil" @if($slug_area == 'perfil') class='ativo' @endif data-slug="perfil" title="Perfil da Trupe">perfil</a></li>
		<li><a href="conheca-a-trupe/clientes" @if($slug_area == 'clientes') class='ativo' @endif data-slug="clientes" title="Clientes da Trupe">clientes</a></li>
		<li><a href="conheca-a-trupe/equipe" @if($slug_area == 'equipe') class='ativo' @endif data-slug="equipe" title="Equipe da Trupe">equipe</a></li>
	</ul>
</nav>
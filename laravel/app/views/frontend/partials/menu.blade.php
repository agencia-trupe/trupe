<div id="menu-principal">
	<div class="centralizar">
		<a href="#" id="hamburguer-link" title="Abrir Menu Mobile"><img src="assets/images/layout/menu-mobile.png" alt="Abrir Menu"></a>
		<a href="home" title="Página Inicial" id="logo-link"></a>
		<div class="marca">
			<img src="assets/images/layout/marca-trupe.png" alt="Trupe Agência Criativa">
		</div>

		<nav>
			<ul class="superior">
				<li><a href="conheca-a-trupe" title="Conheça a Trupe" @if(str_is('conheca', Route::currentRouteName())) class='ativo' @endif>conheça a trupe</a></li>
				<li><a href="areas-de-atuacao" title="Atuação: Design e Web" @if(str_is('atuacao*', Route::currentRouteName())) class='ativo' @endif>atuação: design & web</a></li>
				<li><a href="trabalhe-conosco" title="Trabalhe Conosco" @if(str_is('trabalheconosco*', Route::currentRouteName())) class='ativo' @endif>trabalhe conosco</a></li>
				<li><a href="contato" title="Contato" @if(str_is('contato', Route::currentRouteName())) class='ativo' @endif>contato</a></li>
			</ul>
			<ul class="inferior">
				<li><a href="home" title="Página inicial" @if(str_is('home', Route::currentRouteName())) class='ativo' @endif>HOME</a></li>
				<li><a href="projetos" title="Nossos Projetos" @if(str_is('projetos*', Route::currentRouteName())) class='ativo' @endif>NOSSOS PROJETOS</a></li>
				<li><a href="criacao-de-sites" title="Criação de Sites" @if(str_is('criacaodesites', Route::currentRouteName())) class='ativo' @endif>CRIAÇÃO DE SITES</a></li>
				<li><a href="comunicacao-e-design" title="Comunicação e Design" @if(str_is('comunicacaoedesign', Route::currentRouteName())) class='ativo' @endif>COMUNICAÇÃO E DESIGN</a></li>
				<li><a href="branding" title="Branding" @if(str_is('branding', Route::currentRouteName())) class='ativo' @endif>BRANDING</a></li>
			</ul>
		</nav>
	</div>
</div>
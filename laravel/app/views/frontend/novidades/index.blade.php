@section('conteudo')

	<div class="centralizar titulo">
		<h1>NOVIDADES</h1>
	</div>
	
	<div id="swap-novidades">
		<div class="faixa-roxa-novidades">
			<div class="centralizar">
				<h2>{{ $detalhe->titulo }}</h2>
				@if($detalhe->imagem)
					<div class="detalhes-com-imagem">
						<div class="imagem">
							<img src="assets/images/novidades/{{ $detalhe->imagem }}" alt="{{ $detalhe->titulo }}">
						</div>
						<div class="texto">
							{{ $detalhe->texto }}
						</div>
					</div>
				@else
					<div class="detalhes-sem-imagem">
						<div class="texto">
							{{ $detalhe->texto }}
						</div>
					</div>
				@endif
			</div>
		</div>
		
		<div class="mais-novidades">
			<div class="centralizar">
				@if(sizeof($listaNovidades) > 1)
					<h3>MAIS</h3>
					<ul>
					@foreach ($listaNovidades as $novidade)
						@if($novidade->id != $detalhe->id)
							<li><a href="novidades/{{ $novidade->slug }}" data-slug="{{ $novidade->slug }}" title="{{ $novidade->titulo }}">{{ $novidade->titulo }}</a></li>
						@endif
					@endforeach
					</ul>
				@endif
			</div>
		</div>
	</div>

@stop
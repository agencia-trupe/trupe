@section('conteudo')

	<div class="centralizar contato">
		<div class="titulo">
			<h1>CONTATO</h1>
		</div>
		<section>
			<aside>
				<h1>11 2861&middot;1590</h1>
				<h2>
					Rua Coronel Diogo 762<br>
					Aclimação - São Paulo, SP<br>
					01545-001
				</h2>
			</aside>
			<div class="form">
				<h2>FALE CONOSCO ENVIE &middot; AQUI SUA MENSAGEM</h2>
				<h3>Aceitamos solicitações de orçamento, críticas, sugestões, dúvidas e até elogios!!</h3>
				<form id="formContato" action="contato" method="POST" autocomplete="off" @if(Session::has('contatoEnviado')) class='no-js-form-enviado' @endif>
					<div class="form-interno">
						<ol class="perguntas">
							<li id="q1" data-proximo="2">
								<span><label for="q1-input">Qual é o seu nome?</label></span>
								<div class="input">
									<input name="nome" id="q1-input" type="text" required data-mensagem="Informe seu nome!">
								</div>
							</li>
							<li id="q2" data-proximo="3">
								<span><label for="q2-input">Qual é o seu e-mail?</label></span>
								<div class="input">
									<input name="email" id="q2-input" type="email" required data-mensagem="Informe seu email!">
								</div>
							</li>
							<li id="q3" data-proximo="4">
								<span><label for="q3-input">Qual é o seu telefone de contato?</label></span>
								<div class="input">
									<input name="telefone" id="q3-input" type="text" required data-mensagem="Informe seu telefone para contato!">
								</div>
							</li>
							<li id="q4" data-proximo="fim">
								<span><label for="q4-input">O que você quer nos dizer?</label></span>
								<div class="input">
									<input name="mensagem" id="q4-input" type="text" required data-mensagem="Informe sua mensagem!">
								</div>
							</li>
						</ol>
						<input type="submit" value="ENVIAR" class="submit">
						<div class="controles">
							<button class="next"></button>
							<div class="progresso">
								<div class="passo" id="passo-1"></div>
								<div class="passo" id="passo-2"></div>
								<div class="passo" id="passo-3"></div>
								<div class="passo" id="passo-4"></div>
							</div>
							<span class="mensagem-erro"></span>
							<span class="navegacao">
								<div><button title="1&ordf; questão" data-voltar="1"></button></div>
								<div><button title="2&ordf; questão" data-voltar="2"></button></div>
								<div><button title="3&ordf; questão" data-voltar="3"></button></div>
								<div><button title="4&ordf; questão" data-voltar="4"></button></div>
							</span>
						</div>
					</div>
				</form>
				<div id="resposta-form" @if(Session::has('contatoEnviado')) class='no-js-form-enviado' @endif>
					Obrigado!<br> Sua mensagem foi enviada com sucesso!<br> Entraremos em contato assim que possível!
				</div>
			</div>
		</section>

		<div class="maps">
			<img src="assets/images/layout/mapa.png" alt="Mapa da Trupe" id="mapa-ss">
			<iframe width="100%" height="490" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Coronel+Diogo+762&amp;aq=&amp;sll=-23.682412,-46.595299&amp;sspn=0.921848,1.584778&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Cel.+Diogo,+762+-+Jardim+Gloria,+S%C3%A3o+Paulo,+01545-001&amp;t=m&amp;z=14&amp;ll=-23.578277,-46.620407&amp;output=embed"></iframe>
		</div>

	</div>

@stop
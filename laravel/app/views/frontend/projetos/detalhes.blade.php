@section('conteudo')

	<div class="barra-voltar">
		<div class="centralizar">
			<div class="show-for-small-only"><h1>NOSSOS PROJETOS</h1></div>
			<a href="projetos" title="Menu de Projetos" class="btn-voltar-topo">MENU DE PROJETOS</a>
		</div>
	</div>

	<div class="detalhes superior">
		<div class="centralizar">
			
			<div class="textual">
				<h1>{{ strip_tags($trabalho->titulo) }}</h1>
				
				<div class="texto">
					{{ $trabalho->texto }}
				</div>
				@if(sizeof($trabalho->itens) > 0)
					<ul class="links">
					@foreach ($trabalho->itens as $item)
						<li><div class="icone"><img src="assets/images/itenstrabalho/{{ $item->icone }}" alt="{{ $item->titulo }}"></div> {{ $item->titulo }}</li>
					@endforeach
					</ul>
				@endif
				@if($trabalho->link)
					<a class="link-externo" href="{{ Tools::prep_url($trabalho->link) }}" target="_blank" title="Visitar o site">VISITAR O SITE</a>
				@endif
			</div>

			<div class="capa">
				<img src="assets/images/projetos/capas/{{ $trabalho->capa }}">
			</div>

			<div class="clear"></div>

			@if($trabalho->reel)
				<div class="reel">
					{{ \Vimeo::embed($trabalho->reel, '652px', null) }}
				</div>
			@endif
		</div>
	</div>

	@if(sizeof($trabalho->imagens) > 0)
		<div class="detalhes inferior">
			<div class="centralizar">
				<div class="btn-circular btn-mais">MAIS <img src="assets/images/layout/seta-abaixo.png" alt="Mais sobre o projeto"> </div>
				<div class="lista-imagens">
					@foreach ($trabalho->imagens as $k => $imagem)
						<div class="scrollme">
							<img src="assets/images/projetos/redimensionadas/{{ $imagem->imagem }}"
								 alt="{{ $imagem->descricao }}"
								 class="animateme"
								 data-when="enter"
								 data-from="0.8"
								 data-to="0.2"
								 data-opacity="0"
								 data-translatex="@if($k%2 == 0) -900 @else 900 @endif"
								 easing="easeinout">
						</div>
					@endforeach
				</div>
				<div style="clear:both"></div>
				<div class="fixed-nav">
					<a href="{{ URL::current() }}#" title="Voltar ao Topo" class="btn-circular btn-topo"><img src="assets/images/layout/seta-topo.png" alt="Mais sobre o projeto"> TOPO</a>
					<a href="projetos" title="Voltar para o Menu" class="btn-circular btn-voltar"><img src="assets/images/layout/seta-voltar.png" alt="Mais sobre o projeto"> MENU</a>
				</div>
			</div>
		</div>
	@endif

@stop
@section('conteudo')
	
	<div class="conteudo-projetos bg-turquesa-escuro">
		<div class="centralizar">
			<div class="row collapse">
			
				<aside class="columns small-12">
					<h1>NOSSOS PROJETOS</h1>
					<nav>
						<ul>
							<li><a href="projetos/todos" data-filtro="todos" title="Todos os Projetos" @if($filtro == 'todos') class="ativo" @endif>&raquo; TODOS</a></li>
							<li><a href="projetos/clientes" data-filtro="clientes" title="Ver Projetos por Cliente" @if($filtro == 'clientes') class="ativo" @endif>&raquo; POR CLIENTE</a></li>
							<?php $contador = 2 ?>
							@if(sizeof($listaTipos) > 0)
								@foreach ($listaTipos as $tipo)
									<li><a href="projetos/{{ $tipo->slug }}" title="{{ $tipo->titulo }}" data-filtro="{{ $tipo->slug }}" @if($filtro == $tipo->slug) class="ativo" @endif>&raquo; {{ $tipo->titulo }}</a></li>
									<?php $contador++ ?>
									@if($contador == ceil($nroTipos/3))
										<?php $contador = 0 ?>
										</ul><ul>
									@endif
								@endforeach
							@endif
							
						</ul>
					</nav>
				</aside>
				
				<div class="mobile-offscreen">
					<div class="show-for-small-only mobile-top">
						<h1>NOSSOS PROJETOS &bull; <span></span></h1>
						<a href="projetos" class="mobile-voltar-projetos" title="Voltar para listagem de Categorias"><img src="assets/images/layout/seta-menuprojetos.png" alt="Voltar para listagem de Categorias">MENU DE PROJETOS</a>
					</div>
					<div id="lista-thumbs" data-filtro-inicial="{{ $filtro }}">

						<!-- STAMP: <div class="item-portfolio banners"></div> -->
						@if(sizeof($listaProjetos))				
							@foreach ($listaProjetos as $projeto)
								<a  href="projetos/detalhes/{{ $projeto->cliente->slug }}/{{ $projeto->slug }}"
									title="{{ $projeto->titulo }}"
									class="item-portfolio thumb-redonda {{ $classes[$projeto->id] }} @if($filtro != 'todos' && !in_array($filtro, $slugs_tipos[$projeto->id])) sem-js-nao-mostrar @endif">
									<div class="imagem"><img src="assets/images/projetos/capas_redondas/{{ $projeto->thumb }}"></div>
									<div class="capa">
										<h1>{{ $projeto->cliente->titulo }}</h1>
										<h2>{{ $projeto->titulo }}</h2>
										<p>{{ $projeto->olho }}</p>
										@if(sizeof($projeto->itens) > 0)
											<ul class="links">
											@foreach ($projeto->itens as $item)
												<li title="{{ $item->titulo }}"><img src="assets/images/itenstrabalho/{{ $item->icone }}" alt="{{ $item->titulo }}"></li>
											@endforeach
											</ul>
										@endif
									</div>
								</a>
							@endforeach
						@endif
						
						@if(sizeof($listaClientes))
							@foreach ($listaClientes as $cliente)
								@if($cliente->apresentacao)
									<a href="projetos/apresentacao/{{ $cliente->apresentacao->slug }}" title="{{ $cliente->apresentacao->titulo }}" class="item-portfolio thumb-quadrada item-tipo-cliente @if($filtro != 'todos' && $filtro != 'clientes') sem-js-nao-mostrar @endif">
										<div class="imagem"><img src="assets/images/clientes/{{ $cliente->imagem }}"></div>							
									</a>
								@endif
							@endforeach
						@endif
					</div>
				</div>

			</div>
		</div>
		
	</div>

@stop

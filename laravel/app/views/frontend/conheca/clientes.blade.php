@section('conteudo')

	<div class="centralizar conheca">
		@include('frontend.partials.menu-conheca')	
	</div>
	<div class="principal">
		<div class="clientes">
			<div class="centralizar">
				<h1>TRAGA SUA EMPRESA PARA FAZER PARTE DA TRUPE. ESTAMOS TE ESPERANDO COM IDEIAS INCRÍVEIS!</h1>

				<div class="listaClientes">
					@if(sizeof($listaClientes))
						@foreach ($listaClientes as $cliente)
							<div class="marca-cliente">
								<img src="assets/images/clientes/{{ $cliente->imagem }}" alt="{{ $cliente->titulo }}" title="{{ $cliente->titulo }}">
							</div>
						@endforeach
					@endif
				</div>
			</div>
		</div>
	</div>

@stop
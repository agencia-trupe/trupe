@section('conteudo')

	<div class="centralizar conheca">
		@include('frontend.partials.menu-conheca')	
	</div>
	<div class="principal">
		<div class="equipe">
			<div class="centralizar">
				<aside>
					<h1>EQUIPE</h1>
					<p>
						Nossa equipe possui designers experientes, especialistas em tecnologia, marketing, comunicação e estratégia.
					</p>
					<p>
						Auxiliamos sua empresa a descobrir e implementar ações inovadoras de comunicação.
					</p>
					<p>
						SIMPLES, EFICAZES E FUNCIONAIS.
					</p>
				</aside>
				<section>
					<h2>PESSOAS CHAVE</h2>
					<div class="destaque">
						<div class="linha">
							<div class="box">
								<div class="imagem">
									<img src="assets/images/equipe/fernando-destaque.png" alt="FERNANDO ROCHA - Planejamento e administração.">
								</div>	
								<div class="texto">
									<p>
										FERNANDO ROCHA - Planejamento e administração. Com formação em marketing e administração faz planejamento, cria ações de estratégia, administra a Trupe e ainda é responsável por atendimento e novos negócios.
									</p>
								</div>
							</div>
						</div>
						<div class="linha">
							<div class="box">
								<div class="imagem">
									<img src="assets/images/equipe/leila-destaque.png" alt="LEILA ARRUDA - Criação e tecnologia.">
								</div>	
								<div class="texto">
									<p>
										LEILA ARRUDA - Criação e tecnologia. Com formação em Processamento de Dados, Moda e Design Digital, é diretora de criação, atende projetos de comunicação integrada e sistemas web, planeja a comunicação e dirige a produção.
									</p>
								</div>
							</div>
						</div>
					</div>

					<p>					
						Na Trupe o atendimento é feito por seus diretores, com conhecimento técnico e ideias objetivas para entender sua empresa e propor a melhor estrátegia de comunicação. Temos uma equipe multidisciplinar que desenvolve todas as peças. Produzimos e programamos realmente, nosso trabalho não é terceirizado. Propomos soluções completas e integradas nas diversas áreas da comunicação corporativa.
					</p>

					<div class="listaEquipe mostrarImagens">
						<div class="linha">
							<div class="titulo">ESTRATÉGIA E PLANEJAMENTO</div>
							<div class="imagens-equipe">
								<img src="assets/images/equipe/equipe-fernando.png" alt="" title="">
								<img src="assets/images/equipe/equipe-leila.png" alt="" title="">
							</div>
						</div>
						<div class="linha">
							<div class="titulo">CRIAÇÃO E DESIGN</div>
							<div class="imagens-equipe">
								<img src="assets/images/equipe/equipe-leila.png" alt="" title="">
								<img src="assets/images/equipe/equipe-luiz.png" alt="" title="">
								<img src="assets/images/equipe/equipe-rafael.png" alt="" title="">
							</div>
						</div>
						<div class="linha">
							<div class="titulo">DESENVOLVIMENTO</div>
							<div class="imagens-equipe">
								<img src="assets/images/equipe/equipe-patrick.png" alt="" title="">
								<img src="assets/images/equipe/equipe-bruno.png" alt="" title="">
								<img src="assets/images/equipe/equipe-luiz.png" alt="" title="">
								<img src="assets/images/equipe/equipe-patrick.png" alt="" title="">
								<img src="assets/images/equipe/equipe-bruno.png" alt="" title="">							
							</div>
							<div class="imagens-equipe segunda-linha">
								<img src="assets/images/equipe/equipe-patrick.png" alt="" title="">
								<img src="assets/images/equipe/equipe-bruno.png" alt="" title="">
								<img src="assets/images/equipe/equipe-luiz.png" alt="" title="">
								<img src="assets/images/equipe/equipe-patrick.png" alt="" title="">
								<img src="assets/images/equipe/equipe-bruno.png" alt="" title="">							
							</div>
						</div>
						<div class="linha">
							<div class="titulo">ADMINISTRAÇÃO</div>
							<div class="imagens-equipe">
								<img src="assets/images/equipe/equipe-fernando.png" alt="" title="">
								<img src="assets/images/equipe/equipe-vinicius.png" alt="" title="">
							</div>
						</div>
						<div class="linha">
							<div class="titulo">OPERACIONAL</div>
							<div class="imagens-equipe">
								<img src="assets/images/equipe/equipe-sheila.png" alt="" title="">
							</div>
						</div>
						<div class="linha">
							<div class="titulo">AGENTE ANTI-ESTRESSE</div>
							<div class="imagens-equipe">
								<img src="assets/images/equipe/equipe-frank.png" alt="" title="">
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>

@stop
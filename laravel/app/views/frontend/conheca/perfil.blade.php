@section('conteudo')

	<div class="centralizar conheca">
		@include('frontend.partials.menu-conheca')
	</div>
	<div class="principal">
		<div class="perfil">
			<div class="faixa-escura">
				<div class="centralizar">
					<aside>
						<h1>
						A TRUPE É UMA<br>
						AGÊNCIA CRIATIVA <span>COMPLETA</span>
						</h1>
						<h2>DESIGN & WEB</h2>
						<p>
							Somos uma agência interativa formada por designers experientes, técnicos e estrategistas. Ajudamos você a descobrir e implementar inovações na comunicação com seus clientes com reflexo positivo dentro da sua empresa. Criamos de maneira singular, funcional, notável, e com altíssima qualidade.
						</p>
						<p>
							Atuamos na criação de peças de comunicação de forma integrada. Da criação da marca à presença online, da definição da linha de comunicação à realização de ações promocionais e propaganda, a Trupe cria, planeja e executa, gerenciando todas as etapas do processo.
						</p>
					</aside>
					<section>
						<p>
							A Trupe foi fundada em 2001, em São Paulo. A partir de 2007 assumiu seu atual formato, e em 2011 transferiu-se para o atual endereço. A agência é comandada pelos sócios Leila Arruda e Fernando Rocha que atuam diretamente junto aos clientes.
						</p>
						<div class="galeria">
							<div class="imagem">
								<img src="assets/images/perfil/komboffice.jpg" alt="Descrição Estrutura">
								
							</div>
							<div class="imagem">
								<img src="assets/images/perfil/komboffice.jpg" alt="Descrição Estrutura">
								
							</div>
							<div class="imagem">
								<img src="assets/images/perfil/komboffice.jpg" alt="Descrição Estrutura">
								
							</div>
							<div class="imagem">
								<img src="assets/images/perfil/komboffice.jpg" alt="Descrição Estrutura">
								
							</div>
							<div class="imagem">
								<img src="assets/images/perfil/komboffice.jpg" alt="Descrição Estrutura">
								
							</div>
							<div class="imagem">
								<img src="assets/images/perfil/komboffice.jpg" alt="Descrição Estrutura">
								
							</div>
						</div>
					</section>	
				</div>
			</div>
			<div class="centralizar">
				<div class="grafico barras-zero">
					<div class="anos">
						<div class="inicial">2007</div>
						<div class="atual">2015 <small>(nossas estimativas)</small></div>
					</div>
					<div class="linha chamadas">
						<div class="inicial">
							<span>CHAMADAS TELEFÔNICAS</span>
							<div class="barra" data-largura="60px"></div>
							<div class="valor">0,5/dia</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">30/dia</div>
						</div>
					</div>
					<div class="linha contatos">
						<div class="inicial">
							<span>CONTATOS VIA SITE</span>
							<div class="barra"></div>
							<div class="valor">1/mês</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">200/mês</div>
						</div>
					</div>
					<div class="linha parceiros">
						<div class="inicial">
							<span>EMPRESAS PARCEIRAS</span>
							<div class="barra"></div>
							<div class="valor">1</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">5</div>
						</div>
					</div>
					<div class="linha resolucao">
						<div class="inicial">
							<span>RESOLUÇÃO DE TELA</span>
							<div class="barra"></div>
							<div class="valor">640 x 480</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">1920 x 1080</div>
						</div>
					</div>
					<div class="linha clientes">
						<div class="inicial">
							<span>CLIENTES ATIVOS</span>
							<div class="barra"></div>
							<div class="valor">3</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">30</div>
						</div>
					</div>
					<div class="linha fds">
						<div class="inicial">
							<span>FINAIS DE SEMANA NO ESCRITÓRIO</span>
							<div class="barra"></div>
							<div class="valor">25</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">4</div>
						</div>
					</div>
					<div class="linha projetos">
						<div class="inicial">
							<span>NOVOS PROJETOS</span>
							<div class="barra"></div>
							<div class="valor">6</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">80</div>
						</div>
					</div>
					<div class="linha cafezinhos">
						<div class="inicial">
							<span>CAFÉZINHOS</span>
							<div class="barra"></div>
							<div class="valor">2/dia</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">20/dia</div>
						</div>
					</div>
					<div class="linha area">
						<div class="inicial">
							<span>ÁREA OCUPADA</span>
							<div class="barra"></div>
							<div class="valor">28 m<sup>2</sup></div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">106 m<sup>2</sup></div>
						</div>
					</div>
					<div class="linha pcs">
						<div class="inicial">
							<span>COMPUTADORES</span>
							<div class="barra"></div>
							<div class="valor">2</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">11</div>
						</div>
					</div>
					<div class="linha gadgets">
						<div class="inicial">
							<span>DIFERENTES GADGETS</span>
							<div class="barra"></div>
							<div class="valor">0</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">5 ou mais</div>
						</div>
					</div>
					<div class="linha visitas">
						<div class="inicial">
							<span>VISITAS A PROSPECTS E CLIENTES</span>
							<div class="barra"></div>
							<div class="valor">menos de 1/mês</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">mais de 10/mês</div>
						</div>
					</div>
					<div class="linha feedback">
						<div class="inicial">
							<span>FEEDBACKS POSITIVOS</span>
							<div class="barra"></div>
							<div class="valor">0,5/mês</div>
						</div>
						<div class="atual">
							<div class="barra"></div>
							<div class="valor">mais de 5/mês</div>
						</div>
					</div>
					<div class="obs">obs.: gráfico sem escala fixa</div>
				</div>
				<div class="texto-inferior">
					<h1>UMA AGÊNCIA COMPACTA. FLEXÍVEL.</h1>
					<h2>MÚLTIPLA.</h2>
					<h3>DO TAMANHO CERTO.</h3>
					<p>
						Nossa proposta é atender empresas e departamentos de marketing de forma completa, desde o plano de comunicação até o desenvolvimento de sites e sistemas e a aplicação de toda a linha de comunicação em peças digitais e impressas.
					</p>
				</div>
			</div>
		</div>
	</div>

@stop
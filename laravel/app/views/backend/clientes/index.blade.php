@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Clientes <a href='{{ URL::route('painel.clientes.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Cliente</a>
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela="clientes">

        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Cliente</th>
				<th>Marca</th>
                <th><span class="glyphicon glyphicon-th-list"></span></th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
                <td>{{ $registro->titulo }}</td>
				<td><img src='assets/images/clientes/{{ $registro->imagem }}' style='max-height:60px'></td>
                <td style="width:220px;">
                    @if(sizeof($registro->apresentacao) == 0)
                        <a href="{{ URL::route('painel.apresentacao.create', array('clientes_id' => $registro->id)) }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="left" title="Cliente sem Apresentação">apresentação <span class="glyphicon glyphicon-warning-sign"></span></a>
                    @else
                        <a href="{{ URL::route('painel.apresentacao.edit', $registro->apresentacao->id) }}" class="btn btn-sm btn-default">apresentação</a>
                    @endif
                    <a href="{{ URL::route('painel.trabalhos.index', array('clientes_id' => $registro->id)) }}" class="btn btn-sm btn-default">trabalhos</a>
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.clientes.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.clientes.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop
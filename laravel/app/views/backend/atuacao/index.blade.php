@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Áreas de Atuação @if(sizeof(Atuacao::all()) < $limiteInsercao) <a href='{{ URL::route('painel.atuacao.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Área de Atuação</a> @endif
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='atuacao'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>
				<th>Ícone</th>
                <th><span class="glyphicon glyphicon-list"></span></th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
				<td>{{ $registro->titulo }}</td>
				<td style="background:#065562"><img src='assets/images/atuacao/{{ $registro->icone }}' style='max-width:150px'></td>
                <td style="width:80px;">
                    <a href="{{ URL::route('painel.servicos.index', array('atuacao_id' => $registro->id)) }}" class="btn btn-sm btn-default">serviços</a>
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.atuacao.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.atuacao.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop
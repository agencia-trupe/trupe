@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Banner
        </h2>  

		<form action="{{URL::route('painel.banners.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>
				
				<div class="well">
					<div class="form-group">
						<label for="inputImagem de Fundo">Imagem de Fundo</label>
						<p class="alert alert-info">
							Tamanho ideal: 605px de largura por 605px de altura.<br>
							O corte circular e a opacidade são controlados pelo CSS
						</p>
						<input type="file" class="form-control" id="inputImagem de Fundo" name="imagem_fundo" required>
					</div>
				</div>
				
				<div class="well">
					<div class="form-group">
						<label for="inputImagem de Destaque">Imagem de Destaque</label>
						<p class="alert alert-info">
							Tamanho máximo: 520 de largura por 310 de altura<br>
							Necessário .PNG com fundo transparente
						</p>
						<input type="file" class="form-control" id="inputImagem de Destaque" name="imagem_destaque" required>
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<input type="text" class="form-control" id="inputTexto" name="texto" @if(Session::has('formulario')) value="{{ Session::get('formulario.texto') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link</label>
					<input type="text" class="form-control" id="inputLink" name="link" @if(Session::has('formulario')) value="{{ Session::get('formulario.link') }}" @endif required>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.banners.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
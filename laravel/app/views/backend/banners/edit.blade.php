@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Banner
        </h2>  

		{{ Form::open( array('route' => array('painel.banners.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="well">
					<div class="form-group">
						@if($registro->imagem_fundo)
							Imagem de Fundo Atual<br>
							<img style="max-width:100%; margin:10px auto;" src="assets/images/banners/{{$registro->imagem_fundo}}"><br>
						@endif
						<label for="inputImagem de Fundo">Trocar Imagem de Fundo</label>
						<p class="alert alert-info">
							Tamanho ideal: 605px de largura por 605px de altura.<br>
							O corte circular e a opacidade são controlados pelo CSS
						</p>
						<input type="file" class="form-control" id="inputImagem de Fundo" name="imagem_fundo">
					</div>
				</div>
				
				<div class="well">
					<div class="form-group">
						@if($registro->imagem_destaque)
							Imagem de Destaque Atual<br>
							<img style="max-width:100%; margin:10px auto;" src="assets/images/banners/{{$registro->imagem_destaque}}"><br>
						@endif
						<label for="inputImagem de Destaque">Trocar Imagem de Destaque</label>
						<p class="alert alert-info">
							Tamanho máximo: 520 de largura por 310 de altura<br>
							Necessário .PNG com fundo transparente
						</p>
						<input type="file" class="form-control" id="inputImagem de Destaque" name="imagem_destaque">
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<input type="text" class="form-control" id="inputTexto" name="texto" value="{{$registro->texto}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link</label>
					<input type="text" class="form-control" id="inputLink" name="link" value="{{$registro->link}}" required>
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.banners.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
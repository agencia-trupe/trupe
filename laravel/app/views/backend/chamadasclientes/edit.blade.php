@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Chamada
        </h2>  

		{{ Form::open( array('route' => array('painel.chamadasclientes.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control textarea_simples" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link</label>
					<input type="text" class="form-control" id="inputLink" name="link" value="{{$registro->link}}" required>
				</div>
				
				<div class="form-group">
					@if($registro->icone)
						Ícone Colorido Atual<br>
						<img src="assets/images/clientes/home/{{$registro->icone}}"><br>
					@endif
					<label for="inputÍcone">Trocar Ícone Colorido</label>
					<input type="file" class="form-control" id="inputÍcone" name="icone">
				</div>
				
				<div class="form-group">
					@if($registro->icone_rosa)
						Ícone Rosa Atual<br>
						<img src="assets/images/clientes/home/rosa/{{$registro->icone_rosa}}"><br>
					@endif
					<label for="inputÍconeRosa">Trocar Ícone Rosa</label>
					<input type="file" class="form-control" id="inputÍconeRosa" name="icone_rosa">
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.chamadasclientes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
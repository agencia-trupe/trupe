@section('conteudo')

    <div class="container add">

      	<h2>
        	Cliente: <span class="text-primary">{{ $cliente->titulo }}</span><br>
        	<span class="subdir1">Editar Apresentação</span>
        </h2>

        <hr>
    	<a href="{{ URL::route('painel.clientes.index') }}" class="btn btn-sm btn-default pull-left" style="margin-right:10px;">&larr; voltar para Clientes</a>

	    {{ Form::open(array('route' => array('painel.apresentacao.destroy', $registro->id), 'method' => 'delete')) }}
			<button type='submit' class='btn btn-danger btn-sm btn-delete'><span class="glyphicon glyphicon-remove-sign"></span> Excluir esta Apresentação</button>
		{{ Form::close() }}
		<hr>

		{{ Form::open( array('route' => array('painel.apresentacao.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					@if($registro->capa)
						Capa Atual<br>
						<img src="assets/images/projetos/capas/{{$registro->capa}}"><br>
					@endif
					<label for="inputCapa">Trocar Capa</label>
					<input type="file" class="form-control" id="inputCapa" name="capa">
				</div>
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<textarea name="olho" class="form-control" id="inputOlho" >{{$registro->olho }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link</label>
					<input type="text" class="form-control" id="inputLink" name="link" value="{{$registro->link}}" >
				</div>
				
				<div class="form-group">
					<label for="inputReel">Reel</label>
					<input type="text" class="form-control" id="inputReel" name="reel" value="{{$registro->reel}}" >
				</div>
				
				<hr>

				<div class="well">
					<h4>Itens desenvolvidos no Trabalho</h4>
					<hr>
					@if(sizeof($listaItens) > 0)
						<div class="itensbox container-fluid">
							<div class="row">
								@foreach ($listaItens as $k => $item)
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<label class="checkbox-inline"><input type="checkbox" name="itens[]" value="{{ $item->id }}" @if(in_array($item->id, $registro->itens->lists('id'))) checked @endif > {{ $item->titulo }}</label>
									</div>
									@if(($k + 1)%3 == 0)
										</div><div class="row">
									@endif
								@endforeach
							</div>
						</div>
					@else
						<h5>- Nenhum item cadastrado</h5>
					@endif
				</div>
				
			</div>

			<input type="hidden" name="clientes_id" value="{{ $cliente->id }}">

			<div class="well">
				<label>Imagens</label>
				
				<div id="multiUpload">
					<div id="icone">
						<span class="glyphicon glyphicon-open"></span>
						<span class="glyphicon glyphicon-refresh"></span>
					</div>
					<p>
						Escolha as imagens do Projeto. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
						Se preferir também pode utilizar o botão abaixo para selecioná-las.
					</p>
					<input id="fileupload" type="file" name="files[]" data-url="painel/multi-upload" multiple>					
				</div>
				<div id="listaImagens">
					@if(sizeof($registro->imagens))
						@foreach($registro->imagens as $k => $v)
							<div class='projetoImagem'>
					        	<img src='assets/images/projetos/thumbs/{{$v->imagem}}'>
					        	<input type='hidden' name='imagem[]' value="{{$v->imagem}}">
					        	<textarea name='descricao[]' class="textarea_simples">{{$v->descricao}}</textarea>
					        	<a href='#' class='btn btn-sm btn-default btn-descricao' title='descrição da imagem'><span class='glyphicon glyphicon-pencil'></span> <strong>descrição</strong></a>
					        	<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>
				        	</div>
						@endforeach
					@endif
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
				    	Você pode clicar e arrastar as imagens para ordená-las.
				  	</div>
				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.clientes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
			
		</form>
    </div>
    
@stop
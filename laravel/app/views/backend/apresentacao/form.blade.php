@section('conteudo')

    <div class="container add">

      	<h2>
        	Cliente: <span class="text-primary">{{ $cliente->titulo }}</span><br>
        	<span class="subdir1">Adicionar Apresentação</span>
        </h2>  

        <hr>
    	<a href="{{ URL::route('painel.clientes.index') }}" class="btn btn-sm btn-default">&larr; voltar para Clientes</a>
		<hr>
		
		<form action="{{URL::route('painel.apresentacao.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputCapa">Capa</label>
					<input type="file" class="form-control" id="inputCapa" name="capa" required>
				</div>
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<textarea name="olho" class="form-control" id="inputOlho" >@if(Session::has('formulario')) {{ Session::get('formulario.olho') }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ Session::get('formulario.texto') }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link</label>
					<input type="text" class="form-control" id="inputLink" name="link" @if(Session::has('formulario')) value="{{ Session::get('formulario.link') }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputReel">Reel</label>
					<input type="text" class="form-control" id="inputReel" name="reel" @if(Session::has('formulario')) value="{{ Session::get('formulario.reel') }}" @endif >
				</div>

				<hr>

				<div class="well">
					<h4>Itens desenvolvidos no Trabalho</h4>
					<hr>
					@if(sizeof($listaItens) > 0)
						<div class="itensbox container-fluid">
							<div class="row">
								@foreach ($listaItens as $k => $item)
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<label class="checkbox-inline"><input type="checkbox" name="itens[]" value="{{ $item->id }}" @if(Session::has('formulario.itens') && is_array(Session::has('formulario.itens')) && in_array($item->id, Session::get('formulario.itens'))) checked @endif > {{ $item->titulo }}</label>
									</div>
									@if(($k + 1)%3 == 0)
										</div><div class="row">
									@endif
								@endforeach
							</div>
						</div>
					@else
						<h5>- Nenhum item cadastrado</h5>
					@endif
				</div>

			</div>

			<input type="hidden" name="clientes_id" value="{{ $cliente->id }}">

			<div class="well">
				<label>Imagens</label>
				
				<div id="multiUpload">
					<div id="icone">
						<span class="glyphicon glyphicon-open"></span>
						<span class="glyphicon glyphicon-refresh"></span>
					</div>
					<p>
						Escolha as imagens do Projeto. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
						Se preferir também pode utilizar o botão abaixo para selecioná-las.
					</p>
					<input id="fileupload" type="file" name="files[]" data-url="painel/multi-upload" multiple>					
				</div>
				<div id="listaImagens"></div>
				<div class="panel panel-default">
					<div class="panel-body">
				    	Você pode clicar e arrastar as imagens para ordená-las.
				  	</div>
				</div>
			</div>

			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.clientes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
		</form>
    </div>
    
@stop
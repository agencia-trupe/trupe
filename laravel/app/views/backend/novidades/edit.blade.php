@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Novidade
        </h2>  

		{{ Form::open( array('route' => array('painel.novidades.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

				<div class="form-group">
					<label for="inputChamadanaHome">Chamada na Home</label>
					<input type="text" class="form-control" id="inputChamadanaHome" name="chamada" value="{{$registro->chamada}}" required>
				</div>

				<div class="well">
					<div class="form-group">
						@if($registro->imagem)
							Imagem Atual<br>
							<img style="max-width:100%; margin:10px auto;" src="assets/images/novidades/{{$registro->imagem}}"><br>
						@endif
						<label for="inputImagem">Trocar Imagem</label>
						<input type="file" class="form-control" id="inputImagem" name="imagem">
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.novidades.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
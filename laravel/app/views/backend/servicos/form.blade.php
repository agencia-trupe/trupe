@section('conteudo')

    <div class="container add">

      	<h2>
      		Área de Atuação: <span class="text-primary">{{ $atuacao->titulo }}</span><br>
        	<span class="subdir1">Adicionar Serviço</span>
        </h2>  

		<form action="{{URL::route('painel.servicos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<input type="hidden" name="atuacao_id" value="{{ $atuacao->id }}">

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ Session::get('formulario.texto') }} @endif</textarea>
				</div>
			</div>

			<hr>
			<div class="well">
				<h4>Projetos Relacionados</h4>
				<hr>
				<?php $contador = 0 ?>
				@if(sizeof($listaClientes) > 0)
					<div class="itensbox container-fluid">
						<div class="row">
							@foreach ($listaClientes as $k => $cliente)
								@if(sizeof($cliente->trabalhos))
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="margin-bottom:5px;">
										<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapse-{{ $cliente->id }}">
											{{ $cliente->titulo }}
										</button>
										<div id="collapse-{{ $cliente->id }}" class="collapse" style="margin-bottom:30px;">
											@foreach($cliente->trabalhos as $job)
												<label class="checkbox-inline">
													<input  type="checkbox"
															name="relacionados[]"
															value="{{ $job->id }}">
														{{ $job->titulo }}
												</label><br>
											@endforeach
										</div>
									</div>
									<?php $contador++ ?>
									@if(($contador + 1)%3 == 0)
										</div><div class="row">
									@endif
								@endif
							@endforeach
						</div>
					</div>
				@else
					<h5>- Nenhum cliente cadastrado</h5>
				@endif
			</div>
			<hr>

			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.servicos.index', array('atuacao_id' => $atuacao->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
		</form>
    </div>
    
@stop
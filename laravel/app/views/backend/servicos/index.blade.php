@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Área de Atuação: <span class="text-primary">{{ $atuacao->titulo }}</span>  <a href='{{ URL::route('painel.servicos.create', array('atuacao_id' => $atuacao->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Serviço</a><br>
        <span class="subdir1">Serviços</span>
    </h2>
    
    <hr>
    <a href="{{ URL::route('painel.atuacao.index') }}" class="btn btn-sm btn-default">&larr; voltar para Áreas de Atuação</a>
    <hr>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='servicos'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>
				<th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
				<td>{{ $registro->titulo }}</td>
				<td>{{ Str::words(strip_tags($registro->texto), 10) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.servicos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.servicos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop
@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Serviço
        </h2>  

		{{ Form::open( array('route' => array('painel.servicos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>

			</div>

			<hr>
			<div class="well">
				<h4>Projetos Relacionados</h4>
				<hr>
				@if(sizeof($listaClientes) > 0)
					<div class="itensbox container-fluid">
						<div class="row">
						<?php $contador = 0 ?>
							@foreach ($listaClientes as $k => $cliente)
								@if(sizeof($cliente->trabalhos))
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapse-{{ $cliente->id }}">
											{{ $cliente->titulo }} <span class="caret"></span>
										</button>
										<div id="collapse-{{ $cliente->id }}" class="collapse" style="margin-bottom:30px;">
											@foreach($cliente->trabalhos as $job)
												<label class="checkbox-inline" style="margin-bottom:3px;">
													<input  type="checkbox" 
															name="relacionados[]"
															value="{{ $job->id }}"
															@if(in_array($job->id, $id_projetos_relacionados)) checked @endif >
													{{ $job->titulo }}
												</label><br>
											@endforeach
										</div>
									</div>
									<?php $contador++ ?>
									@if(($contador)%3 == 0)
										</div><div class="row">
									@endif
								@endif
							@endforeach
						</div>
					</div>
				@else
					<h5>- Nenhum cliente cadastrado</h5>
				@endif
			</div>
			<hr>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.servicos.index', array('atuacao_id' => $registro->atuacao->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop
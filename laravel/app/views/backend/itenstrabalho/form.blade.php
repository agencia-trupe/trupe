@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Item
        </h2>  

		<form action="{{URL::route('painel.itenstrabalho.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>
				
				<div class="well">
					<div class="form-group">
						<label for="inputÍcone">Ícone </label>
						<p>
							<em>
								PNG com fundo transparente<br>
								tamanho máximo : 25px x 25px
							</em>
						</p>
						<input type="file" class="form-control" id="inputÍcone" name="icone" required>
					</div>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.itenstrabalho.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
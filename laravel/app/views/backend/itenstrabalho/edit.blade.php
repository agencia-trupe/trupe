@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Item
        </h2>  

		{{ Form::open( array('route' => array('painel.itenstrabalho.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
					
				<div class="well">
					<div class="form-group">
						@if($registro->icone)
							<h4>Ícone Atual</h4>
							<div class="circulo-pequeno-hover">
								<img src="assets/images/itenstrabalho/{{$registro->icone}}">
							</div>
							<p class="alert alert-warning" style="margin-top:10px;"><em>passe o cursor sobre a imagem para ativar o fundo de contraste</em></p>
						@endif
						<hr>
						<label for="inputÍcone">Trocar Ícone</label>
						<p>
							<em>
								PNG com fundo transparente<br>
								tamanho máximo : 25px x 25px
							</em>
						</p>
						<input type="file" class="form-control" id="inputÍcone" name="icone">
					</div>
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.itenstrabalho.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
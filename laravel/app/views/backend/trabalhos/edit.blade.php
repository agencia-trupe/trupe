@section('conteudo')

    <div class="container add">

      	<h2>
	      	Cliente: <span class="text-primary">{{ Clientes::find($registro->clientes_id)->titulo }}</span>
	      	<span class="subdir1">Editar Trabalho</span>
        </h2>

        <hr>
	    <a href="{{ URL::route('painel.trabalhos.index', array('clientes_id' => $registro->clientes_id)) }}" class="btn btn-sm btn-default">&larr; voltar para Trabalhos</a>
	    <hr>

		{{ Form::open( array('route' => array('painel.trabalhos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputCliente">Cliente</label>					 
					<select	name="clientes_id" class="form-control" id="inputCliente" required>
						<option value="">Selecione</option>
						@if(sizeof($listaClientes) > 0)
							@foreach ($listaClientes as $cliente)
								<option value="{{ $cliente->id }}" @if($cliente->id == $registro->clientes_id) selected @endif>{{ $cliente->titulo }}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>

			<hr>
			<div class="well">
				<h4>Tipo de Trabalho</h4>
				<hr>
				@if(sizeof($listaTipos) > 0)
					<div class="itensbox container-fluid">
						<div class="row">
							@foreach ($listaTipos as $i => $tipo)
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<label class="checkbox-inline" style="margin-bottom:3px;">
										<input type="checkbox" 
											   name="tipos[]" 
											   value="{{ $tipo->id }}"
											   @if(in_array($tipo->id, $lista_ids_tipos_relacionados)) checked @endif >
											{{ $tipo->titulo }}
									</label>
								</div>
								@if(($i + 1)%3 == 0)
									</div><div class="row">
								@endif
							@endforeach
						</div>
					</div>
				@else
					<h5>- Nenhum tipo cadastrado</h5>
				@endif
			</div>
			<hr>
			
			<div class="pad">
				<div class="form-group">
					@if($registro->capa)
						Capa Atual<br>
						<img src="assets/images/projetos/capas/{{$registro->capa}}" style='display:block; margin-bottom:10px;'>
					@endif
					<label for="inputCapa">Trocar Capa para página de detalhes (730x340)</label>
					<input type="file" class="form-control" id="inputCapa" name="capa">
				</div>

				<hr>

				<div class="form-group">
					@if($registro->thumb)
						Capa Atual<br>
						<img src="assets/images/projetos/capas_redondas/{{$registro->thumb}}" style='display:block; margin-bottom:10px;'>
					@endif
					<label for="inputThumb">Trocar Capa para listagem (200x200)</label>
					<input type="file" class="form-control" id="inputThumb" name="thumb">
				</div>

				<hr>
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<br>
					<small>Quebra de linha : &lt;br&gt;</small>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<input type="text" class="form-control" id="inputOlho" name="olho" value="{{$registro->olho}}">
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link para versão que está no ar</label>
					<input type="text" class="form-control" id="inputLink" name="link" value="{{$registro->link}}" >
				</div>
				
				<div class="form-group">
					<label for="inputReel">Reel (URL do Vimeo)</label>
					<input type="text" class="form-control" id="inputReel" name="reel" value="{{$registro->reel}}" >
					@if($registro->reel)
						<br>
						{{ \Vimeo::embed($registro->reel, '600px', null) }}
					@endif
				</div>
								
			</div>
			
			<hr>
			<div class="well">
				<h4>Itens desenvolvidos no Trabalho</h4>
				<hr>
				@if(sizeof($listaItens) > 0)
					<div class="itensbox container-fluid">
						<div class="row">
							@foreach ($listaItens as $k => $item)
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<label class="checkbox-inline" style="margin-bottom:3px;">
										<input  type="checkbox"
												name="itens[]"
												value="{{ $item->id }}"
												@if(in_array($item->id, $lista_ids_itens_relacionados)) checked @endif >
										{{ $item->titulo }}
									</label>
								</div>
								@if(($k + 1)%3 == 0)
									</div><div class="row">
								@endif
							@endforeach
						</div>
					</div>
				@else
					<h5>- Nenhum item cadastrado</h5>
				@endif
			</div>
			
			<input type="hidden" name="trabalhos_id" value="{{ $registro->id }}">

			<div class="well">
				<label>Imagens</label>
				
				<div id="multiUpload">
					<div id="icone">
						<span class="glyphicon glyphicon-open"></span>
						<span class="glyphicon glyphicon-refresh"></span>
					</div>
					<p>
						Escolha as imagens do Projeto. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
						Se preferir também pode utilizar o botão abaixo para selecioná-las.
					</p>
					<input id="fileupload" type="file" name="files[]" data-url="painel/multi-upload" multiple>					
				</div>
				<div id="listaImagens">
					@if(sizeof($registro->imagens))
						@foreach($registro->imagens as $k => $v)
							<div class='projetoImagem'>
					        	<img src='assets/images/projetos/thumbs/{{$v->imagem}}'>
					        	<input type='hidden' name='imagem[]' value="{{$v->imagem}}">
					        	<textarea name='descricao[]' class="textarea_simples">{{$v->descricao}}</textarea>
					        	<a href='#' class='btn btn-sm btn-default btn-descricao' title='descrição da imagem'><span class='glyphicon glyphicon-pencil'></span> <strong>descrição</strong></a>
					        	<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>
				        	</div>
						@endforeach
					@endif
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
				    	Você pode clicar e arrastar as imagens para ordená-las.
				  	</div>
				</div>
			</div>		
			
			<hr>
	
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.trabalhos.index', array('clientes_id' => $registro->clientes_id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop
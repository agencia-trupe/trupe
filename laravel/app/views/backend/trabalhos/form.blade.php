@section('conteudo')

    <div class="container add">

      	<h2>
      		@if(isset($clientes_id) && !is_null($clientes_id))
      			Cliente: <span class="text-primary">{{ Clientes::find($clientes_id)->titulo }}</span>
        		<span class="subdir1">Adicionar Trabalho</span>
        	@else
        		Adicionar Trabalho
      		@endif
        </h2>  

	    <hr>
	    <a href="{{ URL::route('painel.trabalhos.index', array('clientes_id' => $clientes_id)) }}" class="btn btn-sm btn-default">&larr; voltar para Trabalhos</a>
	    <hr>

		<form action="{{URL::route('painel.trabalhos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputCliente">Cliente</label>					 
					<select	name="clientes_id" class="form-control" id="inputCliente" required>
						<option value="">Selecione</option>
						@if(sizeof($listaClientes) > 0)
							@foreach ($listaClientes as $cliente)
								<option value="{{ $cliente->id }}" @if((Session::has('formulario') && Session::get('formulario.clientes_id') == $cliente->id) || (isset($clientes_id) && !is_null($clientes_id) && $clientes_id == $cliente->id)) selected @endif>{{ $cliente->titulo }}</option>
							@endforeach
						@endif
					</select>
				</div>

			</div>


			<hr>
			<div class="well">
				<h4>Tipo de Trabalho</h4>
				<hr>
				@if(sizeof($listaTipos) > 0)
					<div class="itensbox container-fluid">
						<div class="row">
							@foreach ($listaTipos as $i => $tipo)
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<label class="checkbox-inline" style="margin-bottom:3px;"><input type="checkbox" name="tipos[]" value="{{ $tipo->id }}" @if(Session::has('formulario') && is_array(Session::get('formulario.tipos')) && in_array($tipo->id, Session::get('formulario.tipos'))) checked @endif > {{ $tipo->titulo }}</label>
								</div>
								@if(($i + 1)%3 == 0)
									</div><div class="row">
								@endif
							@endforeach
						</div>
					</div>
				@else
					<h5>- Nenhum tipo cadastrado</h5>
				@endif
			</div>
			<hr>
				
			<div class="pad">

				<div class="form-group">
					<label for="inputCapa">Capa para página de detalhes (730x340)</label>
					<input type="file" class="form-control" id="inputCapa" name="capa" required>
				</div>

				<hr>

				<div class="form-group">
					<label for="inputThumb">Capa para listagem (200x200)</label>
					<input type="file" class="form-control" id="inputThumb" name="thumb" required>
				</div>

				<hr>
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<br>
					<small>Quebra de linha : &lt;br&gt;</small>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputOlho">Olho</label>
					<input type="text" class="form-control" id="inputOlho" name="olho" @if(Session::has('formulario')) value="{{ Session::get('formulario.olho') }}" @endif>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ Session::get('formulario.texto') }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link para versão que está no ar</label>
					<input type="text" class="form-control" id="inputLink" name="link" @if(Session::has('formulario')) value="{{ Session::get('formulario.link') }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputReel">Reel (URL do Vimeo)</label>
					<input type="text" class="form-control" id="inputReel" name="reel" @if(Session::has('formulario')) value="{{ Session::get('formulario.reel') }}" @endif >
				</div>

			</div>
			
			<hr>

			<div class="well">
				<h4>Itens desenvolvidos no Trabalho</h4>
				<hr>
				@if(sizeof($listaItens) > 0)
					<div class="itensbox container-fluid">
						<div class="row">
							@foreach ($listaItens as $k => $item)
								<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
									<label class="checkbox-inline" style="margin-bottom:3px;"><input type="checkbox" name="itens[]" value="{{ $item->id }}" @if(Session::has('formulario') && is_array(Session::get('formulario.itens')) && in_array($item->id, Session::get('formulario.itens'))) checked @endif > {{ $item->titulo }}</label>
								</div>
								@if(($k + 1)%3 == 0)
									</div><div class="row">
								@endif
							@endforeach
						</div>
					</div>
				@else
					<h5>- Nenhum item cadastrado</h5>
				@endif
			</div>
				
			<div class="well">
				<label>Imagens</label>
				
				<div id="multiUpload">
					<div id="icone">
						<span class="glyphicon glyphicon-open"></span>
						<span class="glyphicon glyphicon-refresh"></span>
					</div>
					<p>
						Escolha as imagens do Projeto. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
						Se preferir também pode utilizar o botão abaixo para selecioná-las.
					</p>
					<input id="fileupload" type="file" name="files[]" data-url="painel/multi-upload" multiple>					
				</div>
				<div id="listaImagens"></div>
				<div class="panel panel-default">
					<div class="panel-body">
				    	Você pode clicar e arrastar as imagens para ordená-las.
				  	</div>
				</div>
			</div>

			<hr>

			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.trabalhos.index', array('clientes_id' => $clientes_id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop
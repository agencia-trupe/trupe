@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Cliente: <span class="text-primary">{{ $cliente->titulo or "Todos os Clientes" }}</span> <a href='{{ URL::route('painel.trabalhos.create', array('clientes_id' => isset($cliente->id) ? $cliente->id : '' )) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Trabalho</a><br>
        <span class="subdir1">Trabalhos</span>
    </h2>
    
    @if(isset($cliente->titulo))
    <hr>
    <a href="{{ URL::route('painel.clientes.index') }}" class="btn btn-sm btn-default">&larr; voltar para Clientes</a>
    <hr>
    @endif
    
    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='trabalhos'>

        <thead>
            <tr>
                @if(!isset($cliente->titulo))
                    <th>Ordenar</th>
                @endif
                <th>Tipo de Trabalho</th>
				<th>Capa</th>
				<th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                @if(!isset($cliente->titulo))
                    <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
                @endif
                <td>
                    @if(sizeof($registro->tipos))
                        <ul>
                        @foreach($registro->tipos as $tipo)
                            <li>&middot; {{ $tipo->titulo }}</li>
                        @endforeach
                        </ul>
                    @else
                        Não Encontrado
                    @endif
                </td>
				<td><img src='assets/images/projetos/capas/{{ $registro->capa }}' style='max-width:150px'></td>
				<td>{{ strip_tags($registro->titulo) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.trabalhos.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.trabalhos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop
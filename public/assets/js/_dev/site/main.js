/* Media Queries Check*/
window.mobilecheck = function() {
  var check = false;
  (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|android|ipad|playbook|silk|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera,'http://detectmobilebrowser.com/mobile');
  return check;
}

var is_small  = (window.innerWidth <= 400);
var is_medium = (window.innerWidth >= 401 && window.innerWidth <= 760);
var is_large  = (window.innerWidth >= 761 && window.innerWidth <= 980);
var is_xlarge = (window.innerWidth >= 981);
var is_mobile = mobilecheck();
/**********************/

var Trupe = {
	init: function(){
		
		if(is_mobile)
			$('html').addClass('is_mobile');

		this.Navegacao.init();
		this.Template.init();
		Trupe.Navegacao._bindJS(window.location.pathname);
	}
}

Trupe.Navegacao = {

	bindPopState: function(){
		$(window).on('popstate.Navegacao', function() {
			var destino = location.pathname.indexOf('/') == 0 ? location.pathname.slice(1) : location.pathname;
			if(destino == '') destino = 'home';
			
			var urls_menu = [
				'home',
				'projetos',
				'areas-de-atuacao',
				'trabalhe-conosco',
				'contato',
				'conheca-a-trupe',
				'criacao-de-sites',
				'comunicacao-e-design',
				'branding'
			];

			// Verificações de URL
			
			// Se for voltar para um item do menu principal, usar _abrirUrl
			if($.inArray(destino, urls_menu) !== -1){
				Trupe.Navegacao._abrirUrl(destino, false);
		    	return true;
			}

			// Se for voltar para o detalhe de portfolio, usar _abrirDetalhes
			if(destino.substring(0,18) == "projetos/detalhes/" || destino.substring(0,22) == "projetos/apresentacao/"){
				Trupe.Projetos._abrirDetalhes(destino, false);				
				return true;
			}

			// Se for voltar para um item da listagem de portfolio
			// Se já estiver no index, é só filtrar com _executaFiltroIsotope
			// Se estiver em outra página, recarregar index
			
			if(destino.substring(0,8) == "projetos"){
				if($('#lista-thumbs').length)
					Trupe.Projetos._executaFiltroIsotope(destino.substring(9), false);
				else
					Trupe.Navegacao._abrirUrl(destino, false);
				
				return true;
			}

			// Voltando para a página de Serviços
			// Se já estiver em serviços, só abrir, se não abrir url
			var regex = /areas-de-atuacao\/(.+)\/(.+)/;
			var check = destino.match(regex);
			if(check){
				if($('.conteudo-servicos').length){
					Trupe.Atuacao._abrirServico(destino, false, check[2]);
				}else
					Trupe.Navegacao._abrirUrl(destino, false);
				return true;
			}

			// Voltando para a página de Atuação
			// Se já estiver em atuação, só abrir, se não abrir url
			if(destino.substring(0,16) == "areas-de-atuacao"){
				if($('ul.lista-areas').length)
					Trupe.Atuacao._abrirAreaAtuacao(destino.substring(17), false);
				else
					Trupe.Navegacao._abrirUrl(destino, false);
				return true;
			}

			// Se estiver voltando para um item do Conheça
			// Se já estiver em conheça, só abrir a slug, se não abrir a url
			if(destino.substring(0,15) == "conheca-a-trupe"){
				if($('nav#navegacao-conheca').length)
					Trupe.Conheca._abrirConheca(destino, false, destino.substring(16));
				else
					Trupe.Navegacao._abrirUrl(destino, false);
				return true;
			}

			// Se estiver votlando para Novidades
			// Se já estiver em novidades, só abrir slug, se não abrir url
			if(destino.substring(0,9) == "novidades"){
				if($('div.faixa-roxa-novidades').length)
					Trupe.Novidades._abrirNovidade(destino, false, destino.substring(10));
				else
					Trupe.Navegacao._abrirUrl(destino, false);
				return true;
			}
		});
	},

	bindMenuPrincipal: function(){
		$('#menu-principal').off('click.bindMenuPrincipal');
		$('#menu-principal').on('click.bindMenuPrincipal', 'nav a, #logo-link', function(e){
			e.preventDefault();
			var destino = $(this).attr('href');			
			Trupe.Navegacao._abrirUrl(destino);
		});
	},

	bindBurguerButton: function(){
		$('#hamburguer-link').off('click.Navegacao');
		$('#hamburguer-link').on('click.Navegacao', function(e){
			e.preventDefault();
			$('nav').addClass('menu-mobile-aberto');
			setTimeout( function(){
				$('nav').removeClass('menu-mobile-aberto');
			}, 10000);
		});
	},

	_bindJS: function(url){
		if(url.indexOf('home') !== -1 || url == '/'){
			Trupe.Home.init();
		}else if(url.indexOf('projetos') !== -1){
			Trupe.Projetos.init();
		}else if(url.indexOf('areas-de-atuacao') !== -1){
			Trupe.Atuacao.init();
		}else if(url.indexOf('trabalhe-conosco') !== -1){
			Trupe.TrabalheConosco.init();
		}else if(url.indexOf('contato') !== -1){
			Trupe.Contato.init();
		}else if(url.indexOf('conheca-a-trupe') !== -1){
			Trupe.Conheca.init();
		}else if(url.indexOf('criacao-de-sites') !== -1){
			Trupe.CriacaoDeSites.init();
		}else if(url.indexOf('comunicacao-e-design') !== -1){
			Trupe.ComunicacaoDesign.init();
		}else if(url.indexOf('branding') !== -1){
			Trupe.Branding.init();
		}else if(url.indexOf('novidades') !== -1){
			Trupe.Novidades.init();
		}	
		$(document).foundation({
			equalizer : { equalize_on_stack: true}
		});
	},

	_abrirUrl: function(destino, armazenar_historico, scroll){
		
		armazenar_historico = armazenar_historico === false ? false : true;

		$.ajax(destino,{
			beforeSend : function(){

				if(destino.indexOf('/') !== -1)
					marcar_destino = destino.substr(0, destino.indexOf('/'));
				else
					marcar_destino = destino;

				$('#menu-principal nav a.ativo').removeClass('ativo');
				$('#menu-principal nav a[href='+marcar_destino+']').addClass('ativo');
			},
			success : function(resposta){
				$('#conteudo-swap').fadeOut('normal', function(){

					if(armazenar_historico)
						history.pushState(null, null, destino);
					
					if(destino == 'home'){
						$('header').addClass('home');
						scroll = 0;
					}
					else
						$('header').removeClass('home');

					$('#conteudo-swap').html(resposta);
					Trupe.Navegacao._scrollTopo(scroll);
					
					// Definindo o delay para transição de acordo com
					// a quantidade de scroll
					// 350 é o tempo default da animação _scrollTop
					if($(window).scrollTop() > 190)
						var delay = 350;
					else
						var delay = 10;

					setTimeout(function(){
						$('#conteudo-swap').fadeIn('normal', function(){
							Trupe.Navegacao._bindJS(destino);

							$('nav').removeClass('menu-mobile-aberto');
							if($('#trupe-html-body').length == 0){
								location.reload(true);
							}
						});
					}, delay);
				});
			}
		});
	},

	_scrollTopo: function(upTo, scrollDown){
		var scroll_padrao = 190;
		var atual = $(window).scrollTop();

		upTo = (upTo == undefined) ? scroll_padrao : upTo;
		scrollDown = (scrollDown === true) ? true : false;
		
		if(is_mobile){
			window.scrollTo(0,0);
		}else{
			if(atual > upTo){
				
				if($('html').scrollTop()) {
				    $('html').animate({ scrollTop: upTo }, 350);
				    return;
				}

				$('body').animate({ scrollTop: upTo }, 350);
			}else{
				
				if(scrollDown === true || 1 == 1){
					
					if($('html').scrollTop()) {					
					    $('html').animate({ scrollTop: upTo }, 350);
					    return;
					}
					$('body').animate({ scrollTop: upTo }, 350);
				}
			}
		}
	},

	bindScrollUsuarioCancelaScrollTop: function(){
		$('body,html').bind('scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove', function(e){
            if ( e.which > 0 || e.type == "mousedown" || e.type == "mousewheel" || e.type == "touchmove"){
                $("html,body").stop();
            }
        });
	},

	init: function(){
		this.bindMenuPrincipal();
		this.bindPopState();
		
		if(is_small || is_medium)
			this.bindBurguerButton();
		//this.bindScrollUsuarioCancelaScrollTop();
	}
};

Trupe.Template = {

	transicaoTextoTopo: function(){
		$('#linksTopo').cycle({
			speed 	: 500,
			timeout : 2000,
			pause 	: true,
			after	: function(el, next_el) {
		        $(next_el).addClass('activeSlide');
		    },
		    before	: function(el) {
		        $(el).removeClass('activeSlide');
		    }
		});
	},

	bindLinks: function(){
		var links_arr = [
			'#linksTopo',
			'footer'
		];
		var links_str = links_arr.join(',');
		$(links_str).off('click.Template');
		$(links_str).on('click.Template', 'a', function(e){
			e.preventDefault();
			Trupe.Navegacao._abrirUrl($(this).attr('href'));
		});
	},

	bindFormFooter: function(){
		$('footer form').submit( function(e){
			e.preventDefault();
			var nome 	 = $('#footer-contato-nome').val(),
				email 	 = $('#footer-contato-email').val(),
				telefone = $('#footer-contato-telefone').val(),
				mensagem = $('#footer-contato-mensagem').val(),
				re 		 = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
				erro 	 = '',
				foco;

			if($('.tooltipstered').length)
				$('.tooltipstered').tooltipster('destroy');

			if(nome == ''){
				erro = 'Informe seu nome';
				foco = '#footer-contato-nome';
			}
			
			if(erro == '' && email == ''){
				erro = 'Informe seu e-mail';
				foco = '#footer-contato-email';
			}
			
			if(erro == '' && !re.test(email)){
				erro = 'Informe um e-mail válido!';
				foco = '#footer-contato-email';
			}
			
			if(erro == '' && mensagem == ''){
				erro = 'Informe sua mensagem!';
				foco = '#footer-contato-mensagem';
			}
			
			if(erro != ''){
				$(foco).tooltipster({
					content : erro,
					trigger : 'click'
				}).click();
				return false;
			}

			$.post('contato', {
				nome 		: nome,
				email 		: email,
				telefone 	: telefone,
				mensagem 	: mensagem
			},function(reposta){
				$('footer form').fadeOut('normal', function(){
					$('#resposta-footer-form').fadeIn();
					setTimeout( function(){
						$('#resposta-footer-form').fadeOut('normal', function(){
							$('footer form').fadeIn('normal');
						})
					}, 5000);
				});
			});
		});
	},

	fixarMenu: function(){
		$(window).scroll(function() {
			if ($(this).scrollTop() >= 190) {
				if(!$('header').hasClass('sticky'))
		    		$('header').addClass('sticky');
		  	}else{
		    	if($('header').hasClass('sticky'))
		    		$('header').removeClass('sticky');
		  	}
		});
	},

	init: function(){
		this.transicaoTextoTopo();
		this.bindLinks();
		this.bindFormFooter();
		
		if((is_large || is_xlarge) && !is_mobile)
			this.fixarMenu();

		$('footer form').attr('novalidate', 'true');
	}
};

Trupe.Home = {

	bindLinksHome: function(){
		var links_parents_arr = [
			'#banners .banner',
			'div.clientes-home',
			'aside#lista-servicos ul li',
			'.cards-home .card-2',
			'.cards-home .card-4',
			'.cards-home .card-5'
		];
		var links_parents_str = links_parents_arr.join(',');

		$(links_parents_str).off('click.Home');
		$(links_parents_str).on('click.Home', 'a', function(e){
			e.preventDefault();
			Trupe.Navegacao._abrirUrl($(this).attr('href'));
		})
	},

	animacaoBanners: function(){
		$('#banners').cycle('destroy');
		$('#banners').cycle({
			cleartypeNoBg : true,
			speed : 650,
			timeout : 10000,
			pause: true,
			pager:  '#bannersControl .centralizar',
		    pagerAnchorBuilder: function(idx, slide) { 
		        return '<a href="#"></a>';
		    },
		    before: function(el, next_el) {
		        $(next_el).addClass('antes');
		        $(el).addClass('depois');
		    },
		    after: function(el, next_el) {
		        $(next_el).removeClass('antes');
		      	$(el).removeClass('depois');		      	
		    },
		});

		$(window).off('resize.animacaoBanners');
		$(window).on('resize.animacaoBanners', function(){
			Trupe.Home.animacaoBanners();
		});

		$(window).off('orientationchange.animacaoBanners');
		$(window).on('orientationchange.animacaoBanners', function(){
			Trupe.Home.animacaoBanners();
		});
	},

	trocaImagemClientes: function(){
		$('.clientes-home a').hover( function(){
			$(this).addClass('adicionaDelay').css('background-image', $(this).attr('data-image-hover'));
		},function(){
			$(this).removeClass('adicionaDelay').css('background-image', $(this).attr('data-image'));
		});
	},

	trackScroll: function(){
		$(window).scroll(function (event) {
		    var scroll = $(window).scrollTop();
		    if(scroll >= 1130){
		    	if($('.cards-home').hasClass('fechados'))
		    		$('.cards-home').removeClass('fechados');
		    }
		});
	},

	init: function(){
		this.bindLinksHome();
		this.animacaoBanners();
		this.trocaImagemClientes();
		this.trackScroll();
	},
};

Trupe.Projetos = {

	hoverIntent: function(){
		$('.item-portfolio.thumb-redonda').each( function(){
			$(this).hoverdir({
				speed : 300,
				easing : 'ease-out',
				hoverDelay : 0,
				inverse : false,
				hoverElem : '.capa'
			});
		});
	},

	tooltip: function(){
		if($('.tooltipstered').length)
			$('.item-portfolio.thumb-redonda .links li').tooltipster('destroy');
 		$('.item-portfolio.thumb-redonda .links li').tooltipster();
	},

	bindScrollTop: function(){
		$(document).off('click.bindScrollTop');
		$(document).on('click.bindScrollTop', '.detalhes .btn-topo', function(e){
			e.preventDefault();
			Trupe.Navegacao._scrollTopo();			
		});
	},

	esconderTitles: function(){
		$('.item-portfolio.thumb-redonda').each(function(){
		    $(this).data('original-title', $(this).attr('title'));
		}).hover(
		    function () { 
		        $(this).attr('title','')
		    }, function () { 
		        $(this).attr('title',$(this).data('original-title'))
		});
	},

	masonryThumbs: function(){
		var filtro_string,
			filtro_inicial = $('#lista-thumbs').attr('data-filtro-inicial');
		
		if('clientes' == filtro_inicial)
			filtro_string = '.item-tipo-cliente'
		else if('todos' == filtro_inicial)
			filtro_string = ':not(.item-tipo-cliente)'
		else 
			filtro_string = '.item-tipo-'+filtro_inicial
		
		$('#lista-thumbs').isotope({
			itemSelector: 	'.item-portfolio',
  			layoutMode	: 	'masonry',
  			filter 		:   filtro_string,
  			masonry 	: 	{
  				columnWidth: $('a.item-portfolio.thumb-redonda:first').outerWidth(true)
  			}
		});

		if('todos' != filtro_inicial){
			$('aside').addClass('mob-offscreen');
			if($('aside nav ul li a.ativo').length)
				$('.mobile-offscreen .mobile-top h1 span').html($('aside nav ul li a.ativo').html().replace('»', ''));
		}
	},

	filtrarIndex: function(){
		$(document).off('click.filtrarIndex');
		$(document).on('click.filtrarIndex', 'aside nav ul li a', function(e){
			e.preventDefault();

			if(!$(this).hasClass('ativo')){			
				var filtro = $(this).data().filtro;
				Trupe.Projetos._executaFiltroIsotope(filtro);
			}
		});
	},

	_executaFiltroIsotope: function(filtro, armazenar_historico){
		var container 	= $('#lista-thumbs');
		var thumbs 		= container.find('.item-portfolio');

		armazenar_historico = armazenar_historico === false ? false : true;
		
		$('aside nav ul li a.ativo').removeClass('ativo');
		$('aside nav ul li a[data-filtro='+filtro+']').addClass('ativo');

		if(armazenar_historico)
			history.pushState(null, null, 'projetos/'+filtro);

		if('clientes' == filtro){
			container.isotope({ filter: '.item-tipo-cliente' });
		}else if('todos' == filtro){
			container.isotope({ filter: ':not(.item-tipo-cliente)' });
		}else{
			container.isotope({ filter: '.item-tipo-'+filtro });
		}
		$('aside').addClass('mob-offscreen');
		$('.mobile-offscreen .mobile-top h1 span').html($('aside nav ul li a.ativo').html().replace('»', ''));
	},

	bindAbrirProjeto: function(){
		$(document).off('click.bindAbrirProjeto');
		$(document).on('click.bindAbrirProjeto', '#lista-thumbs .item-portfolio, #lista-relacao .item-portfolio', function(e){
			e.preventDefault();
			var destino = $(this).attr('href');
			Trupe.Projetos._abrirDetalhes(destino);			
		});
	},

	bindMobileVoltarProjeto: function(){
		$(document).off('click.bindMobileVoltarProjeto');
		$(document).on('click.bindMobileVoltarProjeto', '.mobile-offscreen .mobile-top a', function(e){
			e.preventDefault();
			$('.mob-offscreen').removeClass('mob-offscreen');
		});
	},

	bindFecharProjeto: function(){
		$(document).off('click.bindFecharProjeto');
		$(document).on('click.bindFecharProjeto', '#conteudo-swap .btn-voltar-topo, #conteudo-swap .btn-voltar', function(e){
			e.preventDefault();
			Trupe.Navegacao._abrirUrl('projetos');
		})
	},

	_abrirDetalhes: function(destino, armazenar_historico){
		armazenar_historico = armazenar_historico === false ? false : true;

		$.ajax(destino,{
			beforeSend : function(){
				$('#menu-principal nav a.ativo').removeClass('ativo');
				$('#menu-principal nav a[href=projetos]').addClass('ativo');
			},
			success : function(resposta){
				$('#conteudo-swap').fadeOut('normal', function(){

					$('header').removeClass('home');

					if(armazenar_historico)
						history.pushState(null, null, destino);
					
					$('#conteudo-swap').html(resposta);
					$('#conteudo-swap').fadeIn('normal', function(){
						Trupe.Navegacao._bindJS(destino);
						Trupe.Navegacao._scrollTopo();
						imagesLoaded('.lista-imagens', scrollme.init());						
					});
				});
			}
		});
	},

	mobileDestroyScrollMe: function(){
		$('img.animateme').removeClass('animateme');
	},

	bindResize: function(){
		$(window).off('resize.bindResize');
		$(window).on('resize.bindResize', function(){
			Trupe.Projetos.masonryThumbs();
		});

		$(window).off('orientationchange.bindResize');
		$(window).on('orientationchange.bindResize', function(){
			Trupe.Projetos.masonryThumbs();
		});
	},
	
	init: function(){
		// index
		this.hoverIntent();
		this.tooltip();
		this.esconderTitles();		
		this.masonryThumbs();
		this.filtrarIndex();
		this.bindAbrirProjeto();
		this.bindResize();

		this.bindMobileVoltarProjeto();

		// detalhes
		this.bindScrollTop();
		this.bindFecharProjeto();
		if(is_small || is_medium)
			this.mobileDestroyScrollMe();
	},
};

Trupe.Atuacao = {

	bindDetalhesAtuacao: function(){
		$('.lista-areas > li').off('click.bindDetalhesAtuacao');
		$('.lista-areas > li').on('click.bindDetalhesAtuacao', '.icone-area', function(e){
			e.preventDefault();
			var slug = $(this).data().slug;
			Trupe.Atuacao._abrirAreaAtuacao(slug);
		});
	},

	bindAbrirServicos: function(){
		$('.links .lista-servicos').off('click.bindAbrirServicos')
		$('.links .lista-servicos').on('click.bindAbrirServicos', '.link-servico', function(e){
			e.preventDefault();
			var destino = $(this).attr('href');
			Trupe.Navegacao._abrirUrl(destino);
		});
	},

	bindDetalhesServico: function(){
		$('.conteudo-servicos aside ul li').off('click.bindDetalhesServico');
		$('.conteudo-servicos aside ul li').on('click.bindDetalhesServico', 'a', function(e){
			e.preventDefault();
			
			var slug = $(this).data().slug;
			var destino = $(this).attr('href');
			
			if(!$(this).hasClass('ativo')){
				Trupe.Atuacao._abrirServico(destino, true, slug);
			}
		});
	},

	bindOutrasAtuacoes: function(){
		$('.outras-areas .lista-outras').off('click.bindOutrasAtuacoes');
		$('.outras-areas .lista-outras').on('click.bindOutrasAtuacoes', 'a', function(e){
			e.preventDefault();
			var destino = $(this).attr('href');
			Trupe.Navegacao._abrirUrl(destino);
		});
	},

	bindMobileVoltarAtuacao: function(){
		$('.btn-voltar-atuacao').off('click.bindMobileVoltarAtuacao');
		$('.btn-voltar-atuacao').on('click.bindMobileVoltarAtuacao', 'a', function(e){
			e.preventDefault();
			var destino = $(this).attr('href');
			$('.conteudo-servicos').removeClass('mostrarSection');
		});
	},

	_abrirServico: function(destino, armazenar_historico, slug){

		armazenar_historico = armazenar_historico === false ? false : true;

		$.ajax(destino,{
			beforeSend: function(){
				$('.conteudo-servicos aside ul li a.ativo').removeClass('ativo');
				$('.conteudo-servicos aside ul li a[data-slug='+slug+']').addClass('ativo');
			},
			success : function(resposta){
				$('.conteudo-servicos section').fadeOut('normal', function(){
					
					if(armazenar_historico)
						history.pushState(null, null, destino);
					
					$('header').removeClass('home');

					$('section').html($(resposta).find('section').html());
					$('section').fadeIn('normal', function(){
						Trupe.Navegacao._bindJS(destino);
						if(is_small || is_medium)
							$('.conteudo-servicos').addClass('mostrarSection');
					});
				});
			}
		});		
	},

	_abrirAreaAtuacao: function(slug, armazenar_historico){

		armazenar_historico = armazenar_historico === false ? false : true;

		$('.lista-areas li.aberto').removeClass('aberto');
		$('.lista-areas a[data-slug='+slug+']').parent().addClass('aberto');

		if(!is_small && !is_medium)
			Trupe.Navegacao._scrollTopo(240, true);

		if(armazenar_historico)
			history.pushState(null, null, 'areas-de-atuacao/'+slug);
	},

	init: function(){
		this.bindDetalhesAtuacao();
		this.bindAbrirServicos();
		this.bindDetalhesServico();
		this.bindOutrasAtuacoes();
		this.bindMobileVoltarAtuacao();

		if(is_small || is_medium)
			$('.conteudo-servicos').addClass('mostrarSection');

		// Thumbs de relacionamento com Projetos
		if($('.thumb-redonda').length){
			Trupe.Projetos.hoverIntent();
			Trupe.Projetos.tooltip();
			Trupe.Projetos.esconderTitles();
			Trupe.Projetos.bindAbrirProjeto();
		}
	}
};

Trupe.TrabalheConosco = {

	fileUpload: function(){
		$('#fileupload').fileupload({
            dataType: 'json',
            start: function(e, data){
                $('div.anexar label').html('gravando...');
                $('div.segunda-parte .mensagem-erro-arquivo').removeClass('mostrarerro');
            },
            done: function (e, data) {
            	if(data.result.erros == false){
	            	$('div.anexar label').html(data.result.filename).addClass('arquivoSelecionado');
		            $('#nomeArquivo').val(data.result.filename);
		            $('.submit').focus();
            	}else{
					$('div.segunda-parte .mensagem-erro-arquivo').html(data.result.mensagem)
													   			 .addClass('mostrarerro');
					$('div.anexar label').html('ANEXAR CURRÍCULO');
            	}
            }
        });
	},

	adicionaLinkEmail: function(){
		var container = $('.contem-link-cv');
		var imagem = container.find('img');
		var link = $("<a href='' id='link-email' />").html(imagem);
		container.html(link);

		$('#link-email').off('click.adicionaLinkEmail');
		$('#link-email').on('click.adicionaLinkEmail', function(){
			$(this).attr('href', 'mailto:vagas@trupe.net');
		})
		container.show();
		$('.form').hide();
	},

	init: function(){
		if(is_mobile){
			this.adicionaLinkEmail();
		}else{
			Trupe.Formularios.init('formTrabalheConosco', 6, 'TrabalheConosco');
			this.fileUpload();
		}
	}
}

Trupe.Contato = {

	init: function(){
		Trupe.Formularios.init('formContato', 4, 'Contato');

		if(is_small)
			$('#form-contato').addClass('is_small');
	}
};

Trupe.Formularios = {

	form : null,
	perguntas : null,
	total : null,
	atual : null,
	ultimo : null,
	barra : null,
	passos : null,
	nav : null,
	navlinks : null,

	iniciaFormulario: function(namespace){
		this.form.attr('novalidate', true);
		this.bindClickSeta(namespace);
		//this.bindEnter(namespace);
		this.bindNavegacao(namespace);
		this.bindSubmitForm(namespace);
		this._esconderNaoAtivos(1);
		this._atualizarNavegacao(1);		
	},

	bindClickSeta: function(namespace){
		$('.controles .next').off('click.'+namespace);
		$('.controles .next').on('click.'+namespace, function(e){
			e.preventDefault();
			Trupe.Formularios.proximaPergunta();
		});
	},

	bindNavegacao: function(namespace){
		$('.controles .navegacao div button').off('click.'+namespace);
		$('.controles .navegacao div button').on('click.'+namespace, function(e){
			e.preventDefault();
			var mostrar = $(this).data().voltar;
			if(mostrar != Trupe.Formularios.atual){
				Trupe.Formularios._mostrarPergunta(mostrar)
			}
		});
	},

	bindSubmitForm: function(namespace){
		$('.submit').off('click.'+namespace);
		$('.submit').on('click.'+namespace, function(e){
			e.preventDefault();

			// Verificar se a lista de perguntas 'ol.perguntas' tem a classe 'finalizado',
			// se tiver todos os campos foram validados			
			if($('ol.perguntas').hasClass('finalizado')){
				// Verificar se existe a div .anexar
				// Se existir é o formulário de Trabalhe Conosco e precisa do arquivo
				// Se não existir é o formulário de Contato
				if($('div.anexar').length){
					// Verificar se o currículo foi anexado
					if($('div.anexar label').hasClass('arquivoSelecionado')){
						$('.segunda-parte .mensagem-erro-arquivo').removeClass('mostrarerro');
						
						$.post('trabalhe-conosco', {
							nome 		: $('input#q1-input').val(),
							profissao 	: $('input#q2-input').val(),
							pretensao 	: $('input#q3-input').val(),
							email 		: $('input#q4-input').val(),
							telefone 	: $('input#q5-input').val(),
							porque 		: $('input#q6-input').val(),
							nome_arquivo: $('#nomeArquivo').val()
						},function(reposta){
							$('#formTrabalheConosco').fadeOut('normal', function(){
								$('#resposta-form').fadeIn();							
							});
						});

					}else{
						$('.segunda-parte .mensagem-erro-arquivo').html('Selecione o arquivo de seu currículo!')
														   		  .addClass('mostrarerro');
					}
				}else{
					$.post('contato', {
						nome 		: $('input#q1-input').val(),
						email 		: $('input#q2-input').val(),
						telefone 	: $('input#q3-input').val(),
						mensagem 	: $('input#q4-input').val()
					},function(reposta){
						$('#formContato').fadeOut('normal', function(){
							$('#resposta-form').fadeIn();							
						});
					});
				}
			}else{
				Trupe.Formularios.proximaPergunta();				
			}
		});
	},

	proximaPergunta: function(){
		var perguntas 	= this.perguntas,
			atual 		= this.atual,
			proximo 	= $(perguntas[atual]).data().proximo;
		
		this.ultimo = atual;

		if(this._validacaoCampoVazio(atual)){
			this._esconderErro();
			if(this._validacaoEmail(atual)){
				if(proximo == 'fim'){
					var ultimo = this.ultimo;
					var passo_final = this.ultimo + 1;
					this.atual = passo_final;
					this._atualizarBarra(passo_final);
					this.mostrarMensagemFinal();
					setTimeout( function(){
						Trupe.Formularios._esconderPergunta(ultimo);
					}, 600);
				}else{
					this._mostrarPergunta(proximo);
				}
			}else{
				this._mostrarErro('email');
			}
		}else{
			this._mostrarErro(atual);
		}
	},

	_mostrarPergunta: function(i){
		if($.inArray(i, [2,3,4,5,6]) !== -1)
			this._esconderPergunta(i - 1);

		var mostrar = $(this.perguntas[i]);

		this.atual = i;
		this._atualizarBarra(i);
		this._atualizarNavegacao(i);
		
		if($('ol.perguntas').hasClass('finalizado'))
			$('ol.perguntas').removeClass('finalizado');

		if($(this.form).hasClass('contato-finalizado'))
			$(this.form).removeClass('contato-finalizado');

		mostrar.find('span')
			   .addClass('parabaixo')
			   .parent()
			   .removeClass('naoativo');			   

		setTimeout( function(){
			mostrar.find('span').removeClass('parabaixo');
			mostrar.find('input').focus();
		}, 500)
	},

	_esconderPergunta: function(i){
		var esconder = $(this.perguntas[i]);
		esconder.find('span').addClass('paracima');
		setTimeout(function(){
			esconder.addClass('naoativo').find('span').removeClass('paracima');
		}, 400);
	},

	mostrarMensagemFinal: function(){
		$('ol.perguntas').addClass('finalizado');
		
		if(this.form[0].id == 'formContato'){
			$(this.form).addClass('contato-finalizado');
			setTimeout( function(){
				if(!is_mobile)
					$('.submit').focus();
				else
					$('#formContato').focus();
			}, 50);
		}
	},

	_esconderNaoAtivos: function(ativo){
		$.each(this.perguntas, function(idx, el){
			if(idx != ativo)
				$(el).addClass('naoativo')
		});
	},

	_validacaoCampoVazio: function(i){
		if($(this.perguntas[i]).find('input').val() == '')
			return false;
		else
			return true;
	},

	_validacaoEmail: function(i){
		var input = $(this.perguntas[i]).find('input');
		if(input.attr('type') == 'email'){
			var email = input.val(),
				re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		return re.test(email);
		}else{
			return true;
		}
	},

	_mostrarErro: function(i){
		// Passar mensagens para o html dos elementos
		var mensagem,
			input = $(this.perguntas[i]).find('input');
		if(i == 'email'){
			mensagem = 'Informe um e-mail válido!';
		}else{
			mensagem = input.data().mensagem;
		}
		$('.controles .mensagem-erro').html(mensagem).addClass('aberto');
		input.focus();
	},

	_esconderErro: function(){
		$('.controles .mensagem-erro').removeClass('aberto').html('');
	},

	_atualizarBarra: function(i){
		var atual = $(this.passos[i - 1]);
		if(i > this.ultimo)
			atual.addClass('completo');
		else{
			// voltando
			for (var c = this.passos.length - 1; c >= i; c--) {
				$(this.passos[c]).removeClass('completo');
			};
		}
	},

	_atualizarNavegacao: function(i){
		var atual = $(this.navlinks[i]);
		if(i > this.ultimo)
			atual.addClass('completo');
		else{
			// voltando
			for (var c = this.navlinks.length - 1; c > i; c--) {
				$(this.navlinks[c]).removeClass('completo');
			};
		}
	},

	tooltips: function(){
		if($('.tooltipstered').length)
			$('.controles .navegacao div button').tooltipster('destroy');
 		$('.controles .navegacao div button').tooltipster({
 			position : 'bottom',
 			delay: 50
 		});
	},

	init: function(form, total, namespace){
		this.form 		= $('#'+form);
		this.perguntas	= [null];
		this.total		= total;
		this.atual		= 1;
		this.ultimo		= 0;
		this.barra		= $('.controles .progresso');
		this.passos		= [null];
		this.nav		= $('.controles .navegacao');
		this.navlinks	= [null];

		var p = this.perguntas,
			b = this.passos,
			n = this.navlinks;

		this.form.find('li').map( function(idx, quest){
			p.push(quest);
		});

		this.barra.find('.passo').map( function(idx, pass){
			b.push(pass);
		});

		this.nav.find('div').map( function(idx, navlink){
			n.push(navlink);
		});
		
		this.iniciaFormulario(namespace);
		this.tooltips();
	}
};

Trupe.Conheca = {

	bindNavegacao: function(){
		$('nav#navegacao-conheca ul li a').off('click.NavConheca');
		$('nav#navegacao-conheca ul li a').on('click.NavConheca', function(e){
			e.preventDefault();
			var slug = $(this).data().slug;
			var destino = $(this).attr('href');

			if(!$(this).hasClass('ativo')){
				Trupe.Conheca._abrirConheca(destino, true, slug);
			}
		});
	},

	_abrirConheca: function(destino, armazenar_historico, slug){

		armazenar_historico = armazenar_historico === false ? false : true;

		$.ajax(destino,{
			beforeSend: function(){
				$('nav#navegacao-conheca ul li a.ativo').removeClass('ativo');
				$('nav#navegacao-conheca ul li a[data-slug='+slug+']').addClass('ativo');
			},
			success : function(resposta){
				$('.principal').fadeOut('normal', function(){
					
					if(armazenar_historico)
						history.pushState(null, null, destino);
					
					$('header').removeClass('home');
					
					$('.principal').html($('<div />').append(resposta).find('.principal').html());
					$('.principal').fadeIn('normal', function(){
						Trupe.Navegacao._bindJS(destino);
					});
				});
			}
		});
	},

	abrirGrafico: function(){
		if($('.grafico').length){
			if(!is_small && !is_medium){
				$(window).scroll(function() {
					var grafico = $('.grafico');
					var altura_tela = $(window).height();
					if ($(this).scrollTop() >= 450) {
						if(grafico.hasClass('barras-zero'))
				    		grafico.removeClass('barras-zero');
				  	}else{
				    	if(!grafico.hasClass('barras-zero'))
				    		grafico.addClass('barras-zero');
				  	}
				});
			}else{
				$('.grafico').removeClass('barras-zero');
			}
		}
	},

	init: function(){
		this.bindNavegacao();
		this.abrirGrafico();
	}
};

Trupe.CriacaoDeSites = {

	bindLinks: function(){
		$('.link-final a, .link-destaque a').off('click.CriacaoDeSites');
		$('.link-final a, .link-destaque a').on('click.CriacaoDeSites', function(e){
			e.preventDefault();
			Trupe.Navegacao._abrirUrl($(this).attr('href'));
		});
	},

	init: function(){
		this.bindLinks();
	}
};

Trupe.ComunicacaoDesign = {

	bindLinks: function(){
		$('.link-final a').off('click.ComunicacaoDesign');
		$('.link-final a').on('click.ComunicacaoDesign', function(e){
			e.preventDefault();
			Trupe.Navegacao._abrirUrl($(this).attr('href'));
		});
	},

	init: function(){
		this.bindLinks();
	}
};

Trupe.Branding = {
	
	bindLinks: function(){
		$('.listaBranding a').off('click.Branding');
		$('.listaBranding a').on('click.Branding', function(e){
			e.preventDefault();
			Trupe.Navegacao._abrirUrl($(this).attr('href'));
		});
	},

	init: function(){
		this.bindLinks();
	}
};

Trupe.Novidades = {

	bindAbrirNovidades: function(){
		$('.mais-novidades ul li').off('click.bindAbrirNovidades');
		$('.mais-novidades ul li').on('click.bindAbrirNovidades', 'a', function(e){
			e.preventDefault();
			var destino = $(this).attr('href');
			var slug = $(this).attr('data-slug');
			Trupe.Novidades._abrirNovidade(destino, true, slug);
		});
	},

	_abrirNovidade: function(destino, armazenar_historico, slug){

		armazenar_historico = armazenar_historico === false ? false : true;
		
		$.ajax(destino,{
			success : function(resposta){

				$('#swap-novidades').fadeOut('normal', function(){
					
					if(armazenar_historico)
						history.pushState(null, null, destino);
					
					$('header').removeClass('home');

					var conteudo = $('<div />').append($(resposta));
					$('#swap-novidades').html(conteudo.find('#swap-novidades').html());
					$('#swap-novidades').fadeIn('normal', function(){
						Trupe.Navegacao._bindJS(destino);
						if(!is_small && !is_medium)
							Trupe.Navegacao._scrollTopo(240, true);
					});
				});
			}
		});	
	},

	init: function(){
		this.bindAbrirNovidades();
	}
};

$('document').ready( function(){
	
	Trupe.init();

});
jQuery(function($){
    $.datepicker.regional['pt-BR'] = {
        closeText: 'Fechar',
        prevText: '&#x3c;Anterior',
        nextText: 'Pr&oacute;ximo&#x3e;',
        currentText: 'Hoje',
        monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
        'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
        'Jul','Ago','Set','Out','Nov','Dez'],
        dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

function iconeUploadStart(){
    $('#multiUpload #icone .glyphicon-open').css('opacity', 0);
    setTimeout( function(){
        $('#multiUpload #icone .glyphicon-refresh').css({'display' : 'block', 'opacity': 1});
        $('#multiUpload #icone .glyphicon-open').css('display', 'none');
    }, 350);
}

function iconeUploadStop(){
    $('#multiUpload #icone .glyphicon-refresh').css('opacity', 0);
    setTimeout( function(){
        $('#multiUpload #icone .glyphicon-open').css({'display' : 'block', 'opacity': 1});
        $('#multiUpload #icone .glyphicon-refresh').css('display', 'none');
    }, 350);
}

$('document').ready( function(){

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	bootbox.confirm('Deseja Excluir o Registro?', function(result){
	      	if(result)
	        	form.submit();
	      	else
	        	$(this).modal('hide');
    	});
  	});

    $('table.table-sortable tbody').sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    $('#listaImagens').sortable({
        placeholder: "imagem-placeholder"
    }).disableSelection();

	$('.btn-move').click( function(e){e.preventDefault();});

	$('.datepicker').datepicker();


    if($('#fileupload').length){

        $('#fileupload').fileupload({
            dataType: 'json',
            start: function(e, data){
                iconeUploadStart();
            },
            done: function (e, data) {
                var imagem = "<div class='projetoImagem'>";
                imagem += "<img src='"+data.result.files[0].thumb+"'>";
                imagem += "<input type='hidden' name='imagem[]' value='"+data.result.files[0].filename+"'>";
                imagem += "<textarea name='descricao[]' class='textarea_simples'></textarea>";
                imagem += "<a href='#' class='btn btn-sm btn-default btn-descricao' title='descrição da imagem'><span class='glyphicon glyphicon-pencil'></span> <strong>descrição</strong></a>";
                imagem += "<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>";
                imagem += "</div>";
                $('#listaImagens').append(imagem);
                $('#listaImagens').sortable("refresh");
                iconeUploadStop();
            }
        });

        $(document).on('click', '.projetoImagem a.btn-remover', function(e){
            e.preventDefault();
            var parent = $(this).parent();
            parent.css('opacity', .35);
            setTimeout( function(){
                parent.remove();
            }, 350);
        });

        var timeoutHide;
        
        $(document).on('click', '.projetoImagem a.btn-descricao', function(e){
            e.preventDefault();
            var parent = $(this).parent();
            var textarea = parent.find('textarea');
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                textarea.removeClass('visivel');
            }else{
                $(this).addClass('active');
                textarea.addClass('visivel');
                textarea.focus();
            }
        });

        $(document).on('blur', '.projetoImagem textarea', function(e){
            var parent = $(this).parent();
            var botao = parent.find('.btn-descricao');
            var textarea = parent.find('textarea');
            botao.removeClass('active');
            textarea.removeClass('visivel');
        });
    }

    $('[data-toggle="tooltip"]').tooltip();

    if($('.collapse').length){
        $.each($('input[type=checkbox]:checked'), function(){
            var parent = $(this).parent().parent();
            if(!parent.hasClass('in'))
                parent.addClass('in');
        });
    }

    if($('textarea').length){
        $('textarea').not('.textarea_simples').ckeditor();

        // CKEDITOR.stylesSet.add( 'custom_styles', [
            // Block-level styles
            // { name: 'Paragrafo Branco', element: 'p', styles: { 'margin' : '0', 'color' : '#fff', 'font-weight' : 'normal', 'font-family' : ''Varela Round', sans-serif', 'font-size' : '15px', 'line-height' : '130%' } },            
        // ]);
    }
    	
});	

// CORE
var gulp 	  = require('gulp'),
    concat 	= require('gulp-concat'),
    uglify 	= require('gulp-uglify'),
    rename 	= require('gulp-rename'),
    less 	  = require('gulp-less'),
    changed = require('gulp-changed'),
    cssmin  = require('gulp-cssmin'),
    watch 	= require('gulp-watch'),
    jshint  = require('gulp-jshint'),
    plumber = require('gulp-plumber'),
    imagemin= require('gulp-imagemin');

var config = {
	dev: {
		less_site : 'assets/css/_dev/site/**/*.less',
    less_resp : 'assets/css/_dev/site/responsive/*.less',
		less_adm  : 'assets/css/_dev/painel/*.less',
		js_site	  : [
      'assets/vendor/jquery.cycle/jquery.cycle.all.js',
      'assets/vendor/fancybox/source/jquery.fancybox.js',
      'assets/vendor/foundation/js/foundation.min.js',
      'assets/vendor/foundation/js/foundation/foundation.equalizer.js',
      'assets/vendor/hoverdir/hoverdir.js',
      'assets/vendor/tooltipster/js/jquery.tooltipster.js',
      'assets/vendor/isotope/dist/isotope.pkgd.min.js',
      'assets/vendor/imagesloaded/imagesloaded.pkgd.min.js',
      'assets/vendor/scrollme/jquery.scrollme.min.js',
      'assets/vendor/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
      'assets/vendor/blueimp-file-upload/js/jquery.iframe-transport.js',
      'assets/vendor/blueimp-file-upload/js/jquery.fileupload.js',
      'assets/js/_dev/site/main.js'
    ],
		js_adm	  : 'assets/js/_dev/painel/*.js',
    imgs      : [
      'assets/images/**/*'
    ]
	},
	build:{
		css_site : 'assets/css/',
		css_adm : 'assets/css/painel/',
		js_site	 : 'assets/js/',
		js_adm	 : 'assets/js/painel/'
	}
}

// TASKS

gulp.task('css-painel', function () {
    return gulp.src(config.dev.less_adm)
             .pipe(plumber())
    	       .pipe(less())
    	       .pipe(cssmin())
    	       .pipe(gulp.dest(config.build.css_adm));
});

gulp.task('css-site', function () {
    return gulp.src([config.dev.less_site, config.dev.less_resp])
             .pipe(plumber())
  		       .pipe(concat('main.less'))
    	       .pipe(less())
    	       .pipe(cssmin())
    	       .pipe(gulp.dest(config.build.css_site));
});

gulp.task('css-site-build', ['css-site'], function () {
    return gulp.src(['assets/css/fontface.css',
                     'assets/vendor/foundation/css/normalize.css',
                     'assets/css/foundation.css',
                     'assets/vendor/fancybox/source/jquery.fancybox.css',
                     'assets/vendor/tooltipster/css/tooltipster.css',
                     config.build.css_site+'main.css',])
             .pipe(plumber())
             .pipe(concat('build.css'))
             .pipe(cssmin())
             .pipe(gulp.dest(config.build.css_site));
});

gulp.task('js-painel', function(){
    return gulp.src(config.dev.js_adm)
               .pipe(plumber())
               .pipe(jshint())
               .pipe(uglify())
               .pipe(gulp.dest(config.build.js_adm));
});

gulp.task('js-site', function(){
    return gulp.src(config.dev.js_site)
               .pipe(plumber())
               .pipe(concat('main.js'))
               .pipe(jshint())
               .pipe(uglify())
               .pipe(gulp.dest(config.build.js_site));
});

gulp.task('images', function(){
    return gulp.src(config.dev.imgs, {base : 'assets/images'})
               .pipe(plumber())
               .pipe(imagemin({
                  progressive: true
               }))
               .pipe(gulp.dest('assets/images'));
});

gulp.task('watch', function() {
    gulp.watch(config.dev.less_adm, ['css-painel']);
    gulp.watch(config.dev.less_resp, ['css-site']);
    gulp.watch(config.dev.less_site, ['css-site']);
    gulp.watch(config.dev.js_adm, ['js-painel']);
    gulp.watch(config.dev.js_site, ['js-site']);
});

// TASKS CALL
gulp.task('default', ['css-painel',
					            'css-site',
                      'css-site-build',
                      'js-painel',
                      'js-site',
                      'images']);